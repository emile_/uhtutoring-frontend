# Frontend Localization

## Adding translations for a new component
- For each translation you need to add an entry to all <available-language>.json files in locale-data.
- Start all entries with : <NameOfComponent>.<EntryName>.

## Adding a new language
- Copy a <available-language>.json file, rename it to the abbreviation of the desired language and put it into locale-data.
- Update all entries in the file with the correct translation.
- Add a new entry to each <available-language>.json : Languages.<abbreviation-of-desired-language> with the translation in the desired language.
- Import the file into the localization handler and provide a new entry in messages map of the handler.
- Provide an icon for the language, put it into public/locale-icons and add an entry for the icon too the localization handler.
