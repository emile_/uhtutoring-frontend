import Messages from "./Messages";

// Messages imports
import messages_en from "./locale-data/en.json";
import messages_nl from "./locale-data/nl.json";

/**
 * Contains most localization logic.
 */
export default class LocalizationHandler {

    /**
     * Language entries
     */
    private messages : Map<string, Messages> = new Map([
        ['en', messages_en],
        ['nl', messages_nl],
    ]);

    /**
     * Icon path entries for each language
     */
    private icons : Map<string, string> = new Map([
        ['en', '/locale-icons/en.png'],
        ['nl', '/locale-icons/nl.png'],
    ]);

    /**
     * Constructor
     **/
    constructor() {
        this.getDefaultLanguage = this.getDefaultLanguage.bind(this);
        this.getMessages = this.getMessages.bind(this);
    }

    /**
     * Get the default language set in browser. Use this as a starting point
     * @return default language
     */
    public getDefaultLanguage(): string {
        let currentLanguage: string = "";
        if (localStorage.getItem('language')) {
            currentLanguage = localStorage.getItem('language') as string;
        }
       else {
            currentLanguage = navigator.language;
            if (!this.messages.has(currentLanguage)) {
                if (this.messages.has(currentLanguage.split(/[-_]/)[0])) {    // Language without region code
                    currentLanguage = currentLanguage.split(/[-_]/)[0];
                } else {
                    currentLanguage = "en";
                }
            }
        }
        localStorage.setItem('language', currentLanguage);
        return currentLanguage;
    }

    /**
     * Get the default language set in browser. Use this as a starting point.
     * @return default language
     */
    public getMessages(lang: string): Messages| undefined {
        return this.messages.get(lang);
    }

    /**
     * Get the supported languages.
     * @return array of all supported languages
     */
    public getSupportedLanguages(): string[] {
        return Array.from(this.messages.keys());
    }

    /**
     * Get the icon paths for all supported languages.
     * @return map of icon paths for all supported languages
     */
    public getSupportedLanguagesIcons(): Map<string,string> {
        return this.icons;
    }

}
