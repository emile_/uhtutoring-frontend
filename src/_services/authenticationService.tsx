import axios, {AxiosError} from 'axios';
import { BehaviorSubject } from 'rxjs';
import {Role, stringToRole} from "../_helpers/role";

/**
 * Current user observable
 */
const currentUser = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser') as string));

/**
 * Provides an interface for the authentication service.
 */
export const authenticationService = {
    login,
    logout,
    getToken,
    getID,
    getFirstName,
    getLastName,
    getEmail,
    getRole,
    getProgramme,
    currentUser: currentUser.asObservable(),
    getCurrentUserValue() { return currentUser.value },
};

/**
 * Login
 */
function login(email : string, password : string){
    logout();
    return axios.post(process.env.REACT_APP_BACKEND_API_URL + '/users/login', {'email': email, 'password': password})
        .then((response) => {
                localStorage.setItem('currentUser', JSON.stringify(response.data['data']));
                currentUser.next(response.data);
                return response.status;
            }
        )
        .catch((error : AxiosError) => {
                if(error.response){
                    return error.response.status;
                }
                else {
                    return null;
                }
            }
        )
}

/**
 * Logout
 */
function logout() {
    localStorage.removeItem('currentUser');
    currentUser.next(null);
}

/**
 * Retrieve the authentication token
 */
function getToken() : null | string {
    try {
        return currentUser.value['token'];
    }
    catch (e) {
        return null;
    }
}

/**
 * Retrieve the user's ID
 */
function getID() : null | number {
    try {
        return currentUser.value['user']['id'];
    }
    catch (e) {
        return null;
    }
}

/**
 * Retrieve the user's first name
 */
function getFirstName() : null | string {
    try {
        return currentUser.value['user']['firstname'];
    }
    catch (e) {
        return null;
    }
}

/**
 * Retrieve the user's last name
 */
function getLastName() : null | string {
    try {
        return currentUser.value['user']['lastname'];
    }
    catch (e) {
        return null;
    }
}

/**
 * Retrieve the user's email address
 */
function getEmail() : null | string {
    try {
        return currentUser.value['user']['email'];
    }
    catch (e) {
        return null;
    }
}

/**
 * Retrieve the user's role
 */
function getRole() : null | Role {
    try {
        return stringToRole(currentUser.value['user']['role']);
    }
    catch (e) {
        return null;
    }
}

/**
 * Retrieve the user's programme
 */
function getProgramme() : null | string {
    try {
        return currentUser.value['user']['programme'];
    }
    catch (e) {
        return null;
    }
}
