/**
 * Interface for tutorship
 */
export interface Tutorship {
    "id" : number,
    "tuteeID" : string,
    "tuteeFirstName": string,
    "tuteeLastName": string,
    "tutorID": number,
    "tutorFirstName": string,
    "tutorLastName": string,
    "advertisementID": number,
    "creationTimestamp": string,
    "updateTimestamp": string,
}

/**
 * Interface for session
 */
export interface Session {
    "id" : number,
    "subject" : string,
    "date" : string,
    "tutorshipID" : number,
    "description" : string,
}

/**
 * Interface for review
 */
export interface Review {
    "id": number
    "tutorID": number
    "tutorName": string
    "senderID": number
    "timestamp": string
    "communication": string
    "organization": string
    "content": string
    "extra": string
}

/**
 * Interface for programme
 */
export interface Programme {
    'name' : string
}
