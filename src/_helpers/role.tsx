/**
 * Role enum
 */
export enum Role {
    tutee,
    tutor,
    mentor,
    admin
}

/**
 * Converts Role enum to string.
 * @param role Role enum
 */
export function roleToString(role : Role) : string {
    switch (role) {
        case Role.tutee:
            return "tutee";
        case Role.tutor:
            return "tutor";
        case Role.mentor:
            return "mentor";
        case Role.admin:
            return "admin";
    }
}

/**
 * Converts string to Role enum. ( Default: "" = Role.tutee )
 * @param roleString role string
 */
export function stringToRole(roleString : string) : Role {
    switch (roleString) {
        case "tutee":
            return Role.tutee;
        case "tutor":
            return Role.tutor;
        case "mentor":
            return Role.mentor;
        case "admin":
            return Role.admin;
        default:
            return Role.tutee;
    }
}
