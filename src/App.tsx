import React, { Component } from 'react';
import './App.css';
import {IntlProvider} from 'react-intl';
import Messages from "./_services/localization/Messages";
import LocalizationHandler from "./_services/localization/LocalizationHandler";
import PaperBase from "./components/Theme/PaperBase";
import {MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";


interface State {
    language : string
    messages : Messages | undefined
    authenticationToken : string
    loggedIn : boolean
}

interface Props  {

}

class App extends Component<Props,State> {


    state: State;
    private localizationHandler : LocalizationHandler;

    constructor(props : Props){
        super(props);

        this.localizationHandler = new LocalizationHandler();
        let defaultLanguage = this.localizationHandler.getDefaultLanguage();

        this.state = {
            language : defaultLanguage,
            messages : this.localizationHandler.getMessages(defaultLanguage),
            authenticationToken : "",
            loggedIn : false
        };

        this.onChangeLanguage = this.onChangeLanguage.bind(this);
    };

    /**
     * Callback method when language in NavigationBar was changed.
     * @param lang: language that was selected
     * @post if different language than current, new language has been set together with its messages in state.
     */
    public onChangeLanguage(lang: string): void {
        if (lang !== this.state.language) {
            let newMessages = this.localizationHandler.getMessages(lang);
            if (newMessages) {
                this.setState({
                    language: lang,
                    messages: newMessages,
                });
            }
        }
        localStorage.setItem('language', lang);
    };

    render() {
        return (
            <IntlProvider locale={this.state.language} messages={this.state.messages}>
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <PaperBase
                        language={this.state.language}
                        onChangeLanguage={this.onChangeLanguage}
                        languages={this.localizationHandler.getSupportedLanguages()}
                        languagesIcons={this.localizationHandler.getSupportedLanguagesIcons()}
                    />
                </MuiPickersUtilsProvider>
            </IntlProvider>
        );
    }
}

export default App;
