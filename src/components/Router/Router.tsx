import React, {ComponentClass} from "react";
import {Route, Switch} from "react-router-dom";
import { Chat } from "../Chat/Chat";
import TuteeAdList from "../tutee-ads/TuteeAdList";
import CreateTuteeAd from "../tutee-ads/CreateTuteeAd";
import RegistrationForm from "../registration-form/RegistrationForm";
import LoginForm from "../login/LoginForm";
import {authenticationService} from "../../_services/authenticationService";
import ManageTutors from "../Mentor/TutorManagement/ManageTutors";
import Statistics from "../Mentor/Statistics/Statistics";
import Tutors from "../Mentor/TutorManagement/Tutors";
import TutorShips from "../Mentor/TutorManagement/TutorShips";
import OldAnnouncementList from "../Mentor/Notifications/OldAnnouncementList";
import {Role, roleToString} from "../../_helpers/role";
import Agenda from "../agenda/Agenda";
import AdminPanel from "../Admin/AdminPanel";
import GoogleDrive from "../GoogleDrive/GoogleDrive";
import ConfirmEmail from "../Email Confirmation/ConfirmEmail";
import ReviewForm from "../review-form/ReviewForm";
import Reviews from "../Mentor/Reviews/Reviews";


/**
 * @description A Router, that shows the right UI according to the URL
 * @author Koen laermans
 */
export default function Router() {

    let homePageByRole:{[role:string]:ComponentClass} = {"tutee":TuteeAdList,"tutor":TuteeAdList,"mentor":ManageTutors,"admin":AdminPanel};
    return (

        <main>
            <Switch>
                <Route exact path="/ConfirmEmail" component={ConfirmEmail}/>
                <Route exact path="/Agenda/" component={(authenticationService.getToken() != null) ? Agenda : LoginForm}/>
                <Route exact path="/ReviewTutor/:tutorID" component={(authenticationService.getToken() != null) ? ReviewForm: LoginForm}/>
                <Route exact path="/Reviews" component={(authenticationService.getToken() != null) ? Reviews : LoginForm}/>
                <Route exact path="/GoogleDrive" component={(authenticationService.getToken() != null) ? GoogleDrive : LoginForm}/>
                <Route exact path="/AnnouncementPanel" component={(authenticationService.getToken() != null) ? OldAnnouncementList : LoginForm}/>
                <Route exact path="/Tutorships" component={(authenticationService.getToken() != null) ? TutorShips : LoginForm}/>
                <Route exact path="/ActiveTutors" component={(authenticationService.getToken() != null) ? Tutors : LoginForm}/>
                <Route exact path="/ManageTutors" component={(authenticationService.getToken() != null) ? ManageTutors : LoginForm}/>
                <Route exact path="/Chat" component={(authenticationService.getToken() != null) ? Chat : LoginForm}/>
                <Route exact path="/Registration" component={(authenticationService.getToken() != null) ? TuteeAdList : RegistrationForm}/>
                <Route exact path="/Advertisements" component={(authenticationService.getToken() != null) ? TuteeAdList : LoginForm}/>
                <Route exact path="/CreateAdvertisement" component={(authenticationService.getToken() != null) ? CreateTuteeAd : LoginForm}/>
                <Route exact path="/Statistics" component={(authenticationService.getToken() != null) ? Statistics : LoginForm}/>
                <Route exact path="/" component={(authenticationService.getToken() != null) ? homePageByRole[roleToString(authenticationService.getRole() as Role)] : LoginForm}/>
            </Switch>
        </main>
    );
}
