import axios from 'axios';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import React, { useState, useEffect } from 'react';
import IconButton from '@material-ui/core/IconButton';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import {authenticationService} from "../../_services/authenticationService";
import { useHistory } from "react-router-dom";
import {Badge} from "@material-ui/core";

interface Notification{
    id: number,
    title: string,
    content: string,
    url: string,
    timestamp: string,
    read: boolean
}

const NotificationsDropdown: React.FC = () => {

    //State
    const [anchorEl, setAnchorEl] = useState(null);
    const [notifications, setNotifications] = useState<Notification[]>([]);
    let history = useHistory();

    //NotificationsAPI
    const AUTH_TOKEN = authenticationService.getToken();

    axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

    const NotificationsAPI = axios.create({
	baseURL : process.env.REACT_APP_BACKEND_API_URL + "/notifications",
	responseType : "json",
    });

    const getNotifications = () => {
	NotificationsAPI.get("/").then(response => {
	    setNotifications(response.data.notifications);
	});
    }

    const readNotifications = () => {
	notifications
	    .filter(n => !n.read)
	    .map(n => NotificationsAPI.put("/"+String(n.id)));
	notifications.forEach(n => n.read = true);
    }

    const deleteNotification = (notification : Notification) => {
	NotificationsAPI.delete("/"+String(notification.id));
	setNotifications(notifications.filter(n => n.id !== notification.id))
    }

    const handleClick = (event: any) => {
	setAnchorEl(event.currentTarget);
    }

    const handleClose = (event: any) => {
	setAnchorEl(null);
	readNotifications();
    }

    const handleNotificationClick = (notification: Notification) => {
	if (notification.url) {
	    history.push(notification.url);
	}
	setAnchorEl(null);
	deleteNotification(notification);
    }

    const notificationToBox = (notification: Notification) => {
	return (
	    <Box key={notification.id} m={1}>
		<Button
		    color={notification.read ? "default" : "primary"}
		    key={notification.id}
		    onClick={() => handleNotificationClick(notification)}>
		    <div>
			<Typography align="left" variant="h6">
			    {unescape(notification.title)}
			</Typography>
			<Typography align="left" variant="body1">
			    {unescape(notification.content)}
			</Typography>
		    </div>
		</Button>
	    </Box>
	);
    }

    const noNotifications = () => {
	return (
	    <Box m={1}>
		<Typography>
		    Geen notificaties
		</Typography>
	    </Box>
	);
    }


    const openSocket = () => {
	const AUTH_TOKEN = authenticationService.getToken();
	const ws = new WebSocket(process.env.REACT_APP_BACKEND_WEBSOCKET_URL!);
	ws.onopen = () => {
	    ws.send('{ "type": "subscribe-notifications", "token": "'+ AUTH_TOKEN + '" }');
	    console.log('connecting to notifications');
	};
	ws.onmessage = evt => {
	    // listen to data sent from the websocket server
	    const notification = JSON.parse(evt.data);
	    if (notification.id !== undefined){
        setNotifications(notifications => [...notifications, notification]);
	    }
	};
	ws.onclose = () => {
	    console.log('disconnected')
	};
    }

    const open = Boolean(anchorEl)
    const id = open ? 'notifications-dropdown' : undefined;

    //This gets run only once when the component gets rendered
    useEffect(() => {
	getNotifications();
	openSocket();
    }, []);

    return (
	<div>
	    <IconButton
		aria-label="notifications"
		onClick={handleClick}
	    >

		<Badge badgeContent={notifications.filter(n => !n.read).length} color="secondary" className={"unreadMessagesBadge"}>
			<NotificationsIcon/>
		</Badge>

	    </IconButton>
	    <Popover
		id={id}
		open={open}
		anchorEl={anchorEl}
		onClose={handleClose}
		anchorOrigin={{
		    vertical: 'bottom',
		    horizontal: 'center',
		}}
		transformOrigin={{
		    vertical: 'top',
		    horizontal: 'center',
		}}
	    >
		{notifications.length ? notifications.map(notificationToBox) : noNotifications()}
	    </Popover>
	</div>
    )
};

export default NotificationsDropdown;
