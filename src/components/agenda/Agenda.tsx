import React, {Component, CSSProperties, SyntheticEvent} from 'react';
import {Calendar, momentLocalizer} from 'react-big-calendar';
import moment from "moment";
import 'react-big-calendar/lib/css/react-big-calendar.css'
import {createMuiTheme} from "@material-ui/core/styles";
import {Button} from "@material-ui/core";
import SessionInformationDialog from "./SessionInformationDialog";
import NewSessionDialog from "./NewSessionDialog";
import {authenticationService} from "../../_services/authenticationService";
import axios, {AxiosError} from 'axios';
import {Role} from "../../_helpers/role";
import {Session, Tutorship} from "../../_helpers/backend-data";
import {Redirect} from "react-router-dom";
import {FormattedMessage} from "react-intl";

export interface TutorshipParsed{
    "id" : number,
    "tuteeFullName" : string,
}

export interface Event{
    title: string,
    start: Date,
    end: Date,
    allDay : boolean,
    resource: any,
    description: string,
    id : number
    tutorshipID : number
}
/**
 * Props
 */
interface IProps{}

/**
 * State
 */
interface IState {
    openSessionInformationDialog : boolean
    openNewSessionDialog : boolean
    events: Array<Event>
    event : Event
    tutorships : Array<TutorshipParsed>
    loggedOut : boolean
}

const localizer = momentLocalizer(moment);

export default class Agenda extends Component<IProps,IState> {
    constructor(props : IProps) {
        super(props);
        this.state = {
            openSessionInformationDialog : false,
            openNewSessionDialog : false,
            tutorships :  new Array<TutorshipParsed>(),
            events : new Array<Event>(),
            event : {
                title: "",
                start: new Date(),
                end: new Date(),
                allDay : false,
                resource: null,
                description: "",
                id : -1,
                tutorshipID : -1
            },
            loggedOut : authenticationService.getToken()===null,
        };
        this.openSessionInformationDialog = this.openSessionInformationDialog.bind(this);
        this.openNewSessionDialog = this.openNewSessionDialog.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.retrieveSessions = this.retrieveSessions.bind(this);
        this.postSession = this.postSession.bind(this);
        this.updateSession = this.updateSession.bind(this);
        this.getTuteeFullName = this.getTuteeFullName.bind(this);

        this.retrieveSessions();
        this.retrieveTutorships();
    }

    // Create a theme
    theme = createMuiTheme();

    style : CSSProperties = {
        height : 750,
        marginRight: this.theme.spacing(4),
        marginLeft: this.theme.spacing(5),
    };

    button : CSSProperties = {
        marginBottom : this.theme.spacing(2),
    };

    convertDateToUTC(date : Date) : Date {
        return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    }

    async retrieveSessions(){
        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const SessionsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/sessions",
            responseType : "json",
        });

        let sessions: Array<Session> = (await SessionsAPI.get("/")).data.data;
        let events = new Array<Event>();

        for (let session of sessions){
            let endDate= this.convertDateToUTC(new Date(session.date));
            endDate.setHours(endDate.getHours() + 2);
            let newEvent = {id : session.id, title : session.subject,start : this.convertDateToUTC(new Date(session.date)), end : endDate, allDay : false, resource : null ,description : session.description, tutorshipID : session.tutorshipID};
            events.push(newEvent);
        }

        this.setState({events : events});
    }

    getTuteeFullName(tutorshipID : number) : string{
        let tuteeFullName : string = "";
        for (let tutorship of this.state.tutorships){
            if (tutorship.id === tutorshipID){
                tuteeFullName = tutorship.tuteeFullName;
                break;
            }
        }
        return  tuteeFullName;
    }

    async retrieveTutorships(){
        if (authenticationService.getRole() !== Role.tutor){
            return ;
        }

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const TutorshipsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/tutorships",
            responseType : "json",
        });

        let tutorships: Array<Tutorship> = (await TutorshipsAPI.get("/")).data.data;
        let tutorshipsParsed = new  Array<TutorshipParsed>();

        for (let tutorship of tutorships){
            let tutorshipParsed = {"id" : tutorship.id, "tuteeFullName" : tutorship.tuteeFirstName + ' ' + tutorship.tuteeLastName};
            tutorshipsParsed.push(tutorshipParsed);
        }

        this.setState({tutorships : tutorshipsParsed});
    }

    async postSession(session : Session){
        if (authenticationService.getRole() !== Role.tutor){
            return ;
        }

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;


        await axios.post(process.env.REACT_APP_BACKEND_API_URL + '/sessions/', {"subject" : session.subject, "date" : session.date, "tutorshipID" : session.tutorshipID,"description" : session.description}).then((response) => {
                return response;
            }
        ).catch((error : AxiosError) => {
                if(error.response){
                    return error.response;
                }
                else {
                    return null;
                }
            }
        );

        this.handleClose();
        this.retrieveSessions();
    }

    async updateSession(session : Session){
        if (authenticationService.getRole() !== Role.tutor){
            return ;
        }

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;


        await axios.put(process.env.REACT_APP_BACKEND_API_URL + '/sessions/' + session.id, {"subject" : session.subject, "date" : session.date, "tutorshipID" : session.tutorshipID,"description" : session.description}).then((response) => {
                return response;
            }
        ).catch((error : AxiosError) => {
                if(error.response){
                    return error.response;
                }
                else {
                    return null;
                }
            }
        );


        this.handleClose();
        this.retrieveSessions();
    }

    /**
     * Render new session button if user is a tutor
     */
    renderNewSessionButton(){
        if(authenticationService.getRole() === Role.tutor){
            if (this.state.tutorships.length === 0){
                return <Button color="primary" variant="contained" style={this.button} onClick={this.openNewSessionDialog} disabled><FormattedMessage id="Agenda.sessionButton"/></Button>;
            }
            else {
                return <Button color="primary" variant="contained" style={this.button} onClick={this.openNewSessionDialog} ><FormattedMessage id="Agenda.sessionButton"/></Button>;
            }
        }
    }

    openSessionInformationDialog(event: Event, e : SyntheticEvent){
        this.setState({event : event});
        this.setState({openSessionInformationDialog : true});
    }

    openNewSessionDialog(){
        this.setState({openNewSessionDialog : true});
    }

    handleClose(){
        this.setState({openSessionInformationDialog : false});
        this.setState({openNewSessionDialog : false});
    }

    renderNewSessionDialog(){
        if(this.state.openNewSessionDialog){
            return <NewSessionDialog open={this.state.openNewSessionDialog} tutorships={this.state.tutorships} postSession={this.postSession} handleClose={this.handleClose}/>;
        }
    }

    renderSessionInformationDialog(){
        if(this.state.openSessionInformationDialog){
            return <SessionInformationDialog open={this.state.openSessionInformationDialog} event={this.state.event} updateSession={this.updateSession} getTuteeFullName={this.getTuteeFullName} handleClose={this.handleClose}/>;
        }
    }

    /**
     * Render
     */
    render(){
        // Redirect if not correct role
        if(authenticationService.getRole() !== Role.tutee && authenticationService.getRole() !== Role.tutor){
            return <Redirect to={'/'}/>
        }

        return(
            <div style={this.style}>
                {this.renderNewSessionButton()}
                <Calendar
                    events={this.state.events}
                    step={10}
                    showMultiDayTimes
                    defaultDate={new Date()}
                    localizer={localizer}
                    onSelectEvent={this.openSessionInformationDialog}
                />
                {this.renderSessionInformationDialog()}
                {this.renderNewSessionDialog()}
            </div>
        );
    }
}
