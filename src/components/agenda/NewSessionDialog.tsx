import React, {ChangeEvent} from 'react';
import {createStyles, makeStyles, Theme, withStyles, WithStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import {InputLabel, MenuItem, TextField} from "@material-ui/core";
import {DateTimePicker} from '@material-ui/pickers';
import {TutorshipParsed} from "./Agenda";
import {Session} from "../../_helpers/backend-data";
import moment from "moment";
import {FormattedMessage} from "react-intl";


/**
 * Props
 */
interface IProps{
    open : boolean
    handleClose() : void
    postSession(session : Session) : void
    tutorships : Array<TutorshipParsed>
}

const useStyles = makeStyles(theme => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 400,
    },
    picker: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginTop : theme.spacing(2),
        width: 400,
    },
    inputLabel: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(0.2),
        color: "black"
    },
    formControl: {
        marginLeft: theme.spacing(1),
        paddingTop: theme.spacing(2),
        minWidth: 150,
    },
}));

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}


const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default function NewSessionDialog(props : IProps) {
    // Const containing all styles.
    const classes = useStyles();

    /**
     * States
     */

    const [selectedDate, setSelectedDate] = React.useState<Date>(
        new Date(),
    );

    const [selectedSubject, setSelectedSubject] = React.useState<string>(
        "",
    );

    const [selectedSubjectIncorrect, setSelectedSubjectIncorrect] = React.useState<boolean>(
        false,
    );

    const [selectedDescription, setSelectedDescription] = React.useState<string>(
        "",
    );

    const [selectedTutorship, setSelectedTutorship] = React.useState<number>(
            -1,
    );

    const [selectedTutorshipIncorrect, setSelectedTutorshipIncorrect] = React.useState<boolean>(
        false,
    );


    const handleDateChange = (date: Date | null) => {
        if (date != null){
            setSelectedDate(date);
        }
    };
    function onChangeSelect(event : ChangeEvent<{ name?: string| undefined; value: unknown; }>) {
        let targetValue  = event.target.value;
        for (let tutorship of props.tutorships) {
            if(targetValue === tutorship.id){
                setSelectedTutorship(tutorship.id);
                setSelectedTutorshipIncorrect(false);
                return;
            }
        }
    }

    function returnTuteeSelect() {
        const menuItems = [];
        for (let tutorship of props.tutorships) {
            menuItems.push(<MenuItem value={tutorship.id} key="">{tutorship.tuteeFullName}</MenuItem>);
        }

        return <TextField select helperText={setTutorshipHelperText()} error={selectedTutorshipIncorrect} variant="outlined" className={classes.picker} value={selectedTutorship} onChange={onChangeSelect}  fullWidth={true} >
            {menuItems}
        </TextField>;
    }
    function onChange(event : ChangeEvent<HTMLInputElement>){
        switch (event.currentTarget.name) {
            case 'subject':
                setSelectedSubject(event.currentTarget.value);
                setSelectedSubjectIncorrect(false);
                break;

            case 'description':
                setSelectedDescription(event.currentTarget.value);
                break;

            default:
                break
        }
    }

    function handlePost(){
        let incorrectField : boolean = false;

        if (selectedSubject === ""){
            setSelectedSubjectIncorrect(true);
            incorrectField = true;
        }

        if (selectedTutorship === -1){
            setSelectedTutorshipIncorrect(true);
            incorrectField = true;
        }

        if (incorrectField){
            return;
        }

        let session : Session = {id : -1, subject : selectedSubject, description : selectedDescription, date : moment(selectedDate).format("YYYY-MM-DDTHH:mm:ss.SSS"), tutorshipID : selectedTutorship};
        props.postSession(session);
    }
    function setSubjectHelperText() {
        if(selectedSubjectIncorrect){
            return "Field cannot be empty.";
        }
        else {
            return "";
        }

    }

    function setTutorshipHelperText() {
        if(selectedTutorshipIncorrect){
            return "Please select a tutee.";
        }
        else {
            return "";
        }

    }

    return (
        <Dialog onClose={props.handleClose} aria-labelledby="customized-dialog-title" open={props.open} >
            <DialogTitle id="customized-dialog-title" onClose={props.handleClose}>
                <FormattedMessage id="NewSessionDialog.header"/>
            </DialogTitle>
            <DialogContent dividers>
                <InputLabel className={classes.inputLabel}><FormattedMessage id="NewSessionDialog.subject"/></InputLabel>
                <TextField
                    className={classes.textField}
                    error={selectedSubjectIncorrect}
                    helperText={setSubjectHelperText()}
                    value={selectedSubject}
                    onChange={onChange}
                    name="subject"
                    margin="normal"
                    fullWidth={true}
                    variant="outlined"
                />
                <InputLabel className={classes.inputLabel}><FormattedMessage id="NewSessionDialog.tutee"/></InputLabel>
                {returnTuteeSelect()}
                <InputLabel className={classes.inputLabel}><FormattedMessage id="NewSessionDialog.description"/></InputLabel>
                <TextField
                    className={classes.textField}
                    multiline
                    rows="4"
                    rowsMax="4"
                    name="description"
                    value={selectedDescription}
                    onChange={onChange}
                    margin="normal"
                    fullWidth={true}
                    variant="outlined"
                />
                <InputLabel className={classes.inputLabel}><FormattedMessage id="NewSessionDialog.date"/></InputLabel>
                    <DateTimePicker
                        className={classes.picker}
                        value={selectedDate}
                        onChange={handleDateChange}
                        format="YYYY-MM-DD HH:mm"
                        autoOk
                        ampm={false}
                        fullWidth={true}
                        inputVariant="outlined"
                    />
            </DialogContent>
            <DialogActions>
                <Button autoFocus onClick={handlePost} color="primary">
                    <FormattedMessage id="NewSessionDialog.header"/>
                </Button>
            </DialogActions>
        </Dialog>
    );
}
