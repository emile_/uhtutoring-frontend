import React, {ChangeEvent} from 'react';
import {createStyles, makeStyles, Theme, withStyles, WithStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import {InputLabel, TextField} from "@material-ui/core";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import DateFnsUtils from '@date-io/date-fns';
import {DateTimePicker} from '@material-ui/pickers';
import {Event} from "./Agenda";
import {Session} from "../../_helpers/backend-data";
import moment from "moment";
import {authenticationService} from "../../_services/authenticationService";
import {Role} from "../../_helpers/role";
import {FormattedMessage} from "react-intl";
/**
 * Props
 */
interface IProps{
    open : boolean
    handleClose() : void
    getTuteeFullName(tutorshipID : number) : string
    updateSession(session : Session) : void
    event : Event
}

const useStyles = makeStyles(theme => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 400,
        color: "black"

    },
    textFieldDisabled: {
        color: "black"
    },
    picker: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginTop : theme.spacing(2),
        width: 400,
    },
    inputLabel: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(0.2),
        color: "black"
    },
    formControl: {
        marginLeft: theme.spacing(1),
        paddingTop: theme.spacing(2),
        minWidth: 150,
    },
}));

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}


const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default function SessionInformationDialog(props : IProps) {

    // Const containing all styles.
    const classes = useStyles();

    if (props.event.id === -1){
        props.handleClose();
    }

    const [selectedSubject, setSelectedSubject] = React.useState<string>(
        props.event.title,
    );

    const [selectedSubjectIncorrect, setSelectedSubjectIncorrect] = React.useState<boolean>(
        false,
    );

    const [selectedTutee] = React.useState<string>(
        props.getTuteeFullName(props.event.tutorshipID),
    );

    const [selectedDescription, setSelectedDescription] = React.useState<string>(
        props.event.description,
    );

    const [selectedDate, setSelectedDate] = React.useState<Date>(
        props.event.start,
    );

    function convertDateToUTC(date : Date) : Date {
        return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    }

    let enddate = (moment(props.event.start).add(moment.duration(120, "minutes")).format("YYYYMMDDTHHMMSS"));
    const URL = "http://www.google.com/calendar/event?"
        + "action=TEMPLATE"
        + "&text=" + props.event.title
        + "&dates=" + (moment(props.event.start).format("YYYYMMDDTHHMMSS")) + ("/") +  enddate
        + "&details=" + props.event.description;

    function onChange(event : ChangeEvent<HTMLInputElement>){
        switch (event.currentTarget.name) {
            case 'subject':
                setSelectedSubject(event.currentTarget.value);
                setSelectedSubjectIncorrect(false);
                break;

            case 'description':
                setSelectedDescription(event.currentTarget.value);
                break;

            default:
                break
        }
    }

    const handleDateChange = (date: Date | null) => {
        if (date != null){
            setSelectedDate(date);
        }
    };

    function setSubjectHelperText() {
        if(selectedSubjectIncorrect){
            return "Field cannot be empty.";
        }
        else {
            return "";
        }

    }

    function handleUpdate() {
        let incorrectField : boolean = false;

        if (selectedSubject === ""){
            setSelectedSubjectIncorrect(true);
            incorrectField = true;
        }


        if (incorrectField){
            return;
        }

        let session : Session = {id : props.event.id, subject : selectedSubject, description : selectedDescription, date : moment(selectedDate).format("YYYY-MM-DDTHH:mm:ss.SSS"), tutorshipID : props.event.tutorshipID};
        props.updateSession(session);

    }

    function renderUpdateButton() {
        if (authenticationService.getRole() !== Role.tutor){
            return;
        }
        else {
            return <DialogActions>
            <Button autoFocus onClick={handleUpdate} color="primary">
                <FormattedMessage id="SessionDialog.button"/>
            </Button>
            </DialogActions>;
        }
    }

    function renderTuteeLabel() {
        if (authenticationService.getRole() !== Role.tutor){
            return;
        }
        else {
            return <InputLabel className={classes.inputLabel}><FormattedMessage id="SessionDialog.tutee"/></InputLabel>;
        }
    }

    function renderTuteeField() {
        if (authenticationService.getRole() !== Role.tutor){
            return;
        }
        else {
            return<TextField
            className={classes.textField}
            value={selectedTutee}
            name="subject"
            margin="normal"
            fullWidth={true}
            variant="outlined"
            disabled={true}
            InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
            />;
        }
    }

    // @ts-ignore
    return (
            <Dialog onClose={props.handleClose} aria-labelledby="customized-dialog-title" open={props.open}>
                <DialogTitle id="customized-dialog-title" onClose={props.handleClose}>
                    <FormattedMessage id="SessionDialog.header"/>
                </DialogTitle>
                <DialogContent dividers>
                    <InputLabel className={classes.inputLabel}><FormattedMessage id="SessionDialog.subject"/></InputLabel>
                    <TextField
                        className={classes.textField}
                        error={selectedSubjectIncorrect}
                        helperText={setSubjectHelperText()}
                        value={selectedSubject}
                        onChange={onChange}
                        name="subject"
                        margin="normal"
                        fullWidth={true}
                        variant="outlined"
                        disabled={authenticationService.getRole() !== Role.tutor}
                        InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
                    />
                    {renderTuteeLabel()}
                    {renderTuteeField()}
                    <InputLabel className={classes.inputLabel}><FormattedMessage id="SessionDialog.description"/></InputLabel>
                    <TextField
                        className={classes.textField}
                        multiline
                        rows="4"
                        rowsMax="4"
                        name="description"
                        value={selectedDescription}
                        onChange={onChange}
                        margin="normal"
                        fullWidth={true}
                        variant="outlined"
                        disabled={authenticationService.getRole() !== Role.tutor}
                        InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
                    />
                    <InputLabel className={classes.inputLabel}><FormattedMessage id="SessionDialog.date"/></InputLabel>
                    <DateTimePicker
                        className={classes.picker}
                        value={selectedDate}
                        onChange={handleDateChange}
                        format="YYYY-MM-DD HH:mm"
                        autoOk
                        ampm={false}
                        fullWidth={true}
                        inputVariant="outlined"
                        disabled={authenticationService.getRole() !== Role.tutor}
                        InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
                    />
                    <InputLabel className={classes.inputLabel}><FormattedMessage id="SessionDialog.calender"/></InputLabel>
                    <TextField
                        className={classes.textField}
                        value={URL}
                        margin="normal"
                        fullWidth={true}
                        variant="outlined"
                    />
                </DialogContent>
                {renderUpdateButton()}
            </Dialog>
    );
}
