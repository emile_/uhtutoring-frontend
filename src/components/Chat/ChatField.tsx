import React, { Component, ChangeEvent } from "react";
import axios from "axios";
import { TextField, Button, AppBar, Toolbar, Grid } from "@material-ui/core";
import "./Chat.css"
import {authenticationService} from "../../_services/authenticationService";
import SendIcon from '@material-ui/icons/Send';
import {FormattedMessage} from "react-intl";
interface Props {
    id: number,
}

interface State {
    inputValue: string,
    maxCharCount:number
}

export default class ChatField extends Component<Props, State> {
    render() {
        return (
      <AppBar position="relative" elevation={1} className={"chatBar"}>
        <Toolbar>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs>
                <FormattedMessage id={"chatField.placeholder"}>
                    {translation =>
                      <TextField
                        fullWidth
                        label={translation}
                        multiline={true}
                        onChange={(e) => this.setInputValue(e)}
                        onKeyDown={(e)=>{if (e.key === 'Enter'){this.sendMessage()}}}
                        value={this.state.inputValue}
                        InputProps={{
                          disableUnderline: true,
                        }}
                      />
                    }
                </FormattedMessage>
            </Grid>
            <Grid item className={"chatCount"}>
              {this.state.inputValue.length}/{this.state.maxCharCount}
            </Grid>
            <Grid item>
                <Button
                  aria-label="test"
                  color="primary"
                  onClick={(e) => this.sendMessage()}
                >
                 <SendIcon/>
                </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
        );
    }
    
    constructor (props: Props){
      super(props);
      this.state = {
        inputValue: "",
          maxCharCount:255
      }
    }

    private setInputValue(evt: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {

        let value = (evt.target as HTMLInputElement).value;
        value = value.replace("\n", "");
        if (value.length<=this.state.maxCharCount){
            this.setState({
                inputValue: value,
            });
        }
    }
    private clearInput() {
        this.setState({
          inputValue: "",
        });
    }

    private async sendMessage() {
        const messageContent = this.state.inputValue;

        if (messageContent.length>0){
            this.clearInput();
            const AUTH_TOKEN = authenticationService.getToken();
            axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;
            const MessageAPI = axios.create({
                baseURL : process.env.REACT_APP_BACKEND_API_URL + "/messages/",
                responseType : "json",
            });

            await MessageAPI.post("/" + this.props.id.toString(), {
                content: messageContent,
            });

        }

    }

}
