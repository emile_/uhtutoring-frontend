import React from "react";
import "./Chat.css"
import 'react-chat-elements/dist/main.css';
import { Grid, Paper, Typography} from "@material-ui/core";
import {FormattedMessage} from "react-intl";

interface Props {
    contactName:string
}


/**
    @author Koen laermans
    @description shows a header bar above the chat with the person's name ur chatting with
 */
export default function ChatHeader(props:Props) {

        return (
            <Paper className={"chatHeader"}>
                <Grid container>
                    <Grid item md={10}>
                        <Typography variant={"h6"} className={"contactName"}> <FormattedMessage id={"chatheader.title"}/> {" "+props.contactName}</Typography>
                    </Grid>
                </Grid>
            </Paper>
        );
}
