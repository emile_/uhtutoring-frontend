import React, {Component} from "react";
import axios from "axios";
import { MessageBox } from 'react-chat-elements';
import "./Chat.css"
import 'react-chat-elements/dist/main.css';
import {authenticationService} from "../../_services/authenticationService";

interface Props {
    id: number,
}

interface State {
    messages: Message[],
    id: number
}

interface Message {
    content: string,
    timestamp: string,
    sender: string,
    id: number,
    readByReceiver: number,
    sentByCurrentUser: number,
}

export default class Conversation extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            messages: [],
            id: props.id
        };
        this.scrollToBottom = this.scrollToBottom.bind(this);
        this.conversationRef = React.createRef<HTMLDivElement>();
    }

    conversationRef: any;

    setMessageState(messages: Message[]) {
        messages.sort((a: Message, b: Message) => (a.timestamp > b.timestamp) ? 1 : -1);
        this.setState({
            ...this.state, ...{
                messages,
            },
        });
    }
    addMessage(message: Message) {
        const messages = this.state.messages;
        if (messages[messages.length-1] === undefined || messages[messages.length-1].id !== message.id){
            messages.push(message);
            messages.sort((a: Message, b: Message) => (a.timestamp > b.timestamp) ? 1 : -1);

            this.setState({
                ...this.state, ...{
                    messages,
                },
            });
        }
    }

    componentDidUpdate() {
        this.scrollToBottom()
    }

    scrollToBottom(): void {
        const scrollHeight = this.conversationRef.scrollHeight;
        const height = this.conversationRef.clientHeight;
        const maxScrollTop = scrollHeight - height;
        this.conversationRef.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }

    messageToMessageBox(message: Message) {
        return (
        <div
           key={message.id}
           className={"messageBox"}
        >
          <MessageBox
              position={(message.sentByCurrentUser === 1) ? "right" : "left"}
              type={"text"}
              dateString={message.timestamp.slice(0,10)}
              text={unescape(message.content)}

          />
        </div>
        );
    }

    componentWillReceiveProps(newProps:Props) {
        const id:number = this.state.id;
        if (newProps.id !== id){
            this.setState({
                id:newProps.id
            },
                ()=>{this.componentDidMount();}
            );
        }
    }

    
    initWebsocket(AUTH_TOKEN: string | null, conversationID: number): void {
        const ws = new WebSocket(process.env.REACT_APP_BACKEND_WEBSOCKET_URL!);
        ws.onopen = () => {
          console.log('connected');
          ws.send('{ "type": "subscribe", "conversationID": ' + conversationID.toString() + ', "token": "'+ AUTH_TOKEN + '" }');
          setInterval(() => {
            ws.send('{ "type": "ping", "token": "'+ AUTH_TOKEN + '" }');
          }, 10000);

        };
        ws.onmessage = evt => {
          // listen to data sent from the websocket server
          const message = JSON.parse(evt.data);
          if (message.id !== undefined){
            this.addMessage(message);
          }
        };
        ws.onclose = () => {
          console.log('disconnected');
        };
    }

    async componentDidMount() {
        const AUTH_TOKEN: string | null = authenticationService.getToken();

        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const MessageAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/messages/",
            responseType : "json",
        });

        this.setMessageState((await MessageAPI.get("/" + this.state.id.toString())).data.messages);

        this.initWebsocket(AUTH_TOKEN, this.state.id);

        this.scrollToBottom();
    }

    render() {

        return (
            <div className={"conversation"} ref={(div) => {this.conversationRef = div}}>
                {this.state.messages.map(this.messageToMessageBox)}
            </div>
        );
    }
}
