import React from "react";
import Conversation from "./Conversation";
import ChatField from "./ChatField";
import "./Chat.css"
import {Paper} from "@material-ui/core";
import ChatHeader from "./ChatHeader";

interface Props {
    location:{
        state: {
            id: number,
            name:string
        }
    }

}



export const Chat: React.SFC<Props> = ({location:{state:{id, name}}}) => {
    return (
      <Paper className={"chat"} elevation={3}>
          <ChatHeader contactName={name}/>
          <Conversation id={id}/>
          <ChatField id={id}/>
      </Paper>
    );
};
