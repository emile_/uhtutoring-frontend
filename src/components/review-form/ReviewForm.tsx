import React, {ChangeEvent, Component, CSSProperties} from "react";
import Container from "@material-ui/core/Container";
import {createMuiTheme} from "@material-ui/core/styles";
import {FormattedMessage} from "react-intl";
import Divider from "@material-ui/core/Divider";
import {InputLabel, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {Role} from "../../_helpers/role";
import axios, {AxiosError} from "axios";
import {authenticationService} from "../../_services/authenticationService";
import {Redirect} from "react-router-dom";


/**
 * State
 */
interface IState {
    explanationValue : string
    communicationValue : string
    organisationValue : string
    extraValue : string
    tutorID : string
    maxLengthField : number
    explanationIncorrect : boolean
    communicationIncorrect : boolean
    organisationIncorrect : boolean
    postSucceeded : boolean
}

export default class ReviewForm extends Component<any, IState>{
    constructor(props : any) {
        super(props);
        this.state = {
            explanationValue : "",
            communicationValue:  "",
            organisationValue: "",
            extraValue: "",
            tutorID: this.props.match.params.tutorID,
            maxLengthField: 200,
            explanationIncorrect : false,
            communicationIncorrect : false,
            organisationIncorrect : false,
            postSucceeded : false
        };
        this.setValue = this.setValue.bind(this);
        this.testAllFieldsValid = this.testAllFieldsValid.bind(this);
    }


    // Create a theme
    theme = createMuiTheme();

    /**
     * All styling properties
     */
    styleReviewForm : CSSProperties = {
        boxShadow: '1px 3px 2px -2px rgba(0,0,0,0.2), 1px 2px 2px 1px rgba(0,0,0,0.14), 1px 2px 4px 1px rgba(0,0,0,0.12)'
    };
    styleHeaders: CSSProperties = {
        paddingTop: this.theme.spacing(2),
        textAlign : 'center',
    };
    styleForms: CSSProperties = {
        textAlign: 'left',
        marginTop: this.theme.spacing(2),
        marginLeft: this.theme.spacing(4),
        marginBottom: this.theme.spacing(1),
    };
        styleTextField : CSSProperties = {
        marginLeft: this.theme.spacing(1),
        marginRight: this.theme.spacing(1),
        width: 400,
    };
    styleInputLabel : CSSProperties = {
        paddingTop: this.theme.spacing(2),
        paddingBottom: this.theme.spacing(0.2),
    };
    styleButton : CSSProperties = {
        marginTop: this.theme.spacing(2),
        marginBottom: this.theme.spacing(2),
        marginLeft: this.theme.spacing(1),
    };

    /**
     * Update the value of a field.
     * @param field target field
     * @param value target value
     */
    setValue = (field : string, value : string) => {
        switch (field)
        {
            case 'explanation':
                if(value.length > this.state.maxLengthField){
                    return
                }
                this.setState({explanationValue : value});
                this.setState({explanationIncorrect : false});
                break;

            case 'communication':
                if(value.length > this.state.maxLengthField){
                    return
                }
                this.setState({communicationValue : value});
                this.setState({communicationIncorrect : false});
                break;

            case 'organisation':
                if(value.length > this.state.maxLengthField){
                    return
                }
                this.setState({organisationValue : value});
                this.setState({organisationIncorrect: false});
                break;

            case 'extra':
                if(value.length > this.state.maxLengthField){
                    return
                }
                this.setState({extraValue : value});
                break;

            default:
                break;
        }
    };

    testAllFieldsValid = () : boolean => {
        let allFieldsValid : boolean = true;

        if(this.state.explanationValue === ""){
            allFieldsValid = false;
            this.setState({explanationIncorrect : true});
            console.log('tes');
        }

        if(this.state.communicationValue === ""){
            allFieldsValid = false;
            this.setState({communicationIncorrect : true});
        }

        if(this.state.organisationValue === ""){
            allFieldsValid = false;
            this.setState({organisationIncorrect : true});
        }

        return allFieldsValid;
    };



    /**
     * Post the account details to the back end and create a new account.
     */
    postReview = async () => {

        let allFieldsValid: boolean = this.testAllFieldsValid();

        if (!allFieldsValid) {
            return;
        }

        /**
         * Review parameters
         */
        const tutorID: string = this.state.tutorID;
        const content: string = this.state.explanationValue;
        const communication: string = this.state.communicationValue;
        const organization: string = this.state.organisationValue;
        const extra: string = this.state.extraValue;

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        let response = await axios.post(process.env.REACT_APP_BACKEND_API_URL + '/reviews/' + tutorID, {
            "tutorID": tutorID,
            "communication": communication,
            "organization": organization,
            "content": content,
            "extra": extra
        }).then((response) => {
                return response;
            }
        ).catch((error: AxiosError) => {
                if (error.response) {
                    return error.response;
                } else {
                    return null;
                }
            }
        );

        if (response !== null && response.status === 201){
            this.setState({postSucceeded : true});
        }
    };

    /**
     * When the input of a field has changed.
     * @param event event from input field
     */
    onChange = (event : ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>{
        let targetField : string = event.currentTarget.name;
        let targetValue : string = event.currentTarget.value;
        this.setValue(targetField,targetValue);
    };

    renderHelperText = (incorrectValue : boolean) =>{
            if(incorrectValue){
                return <FormattedMessage id={"ReviewForm.incorrectText"}/>
            }
            else {
                return ""
            }
    };

    render(){
        if(authenticationService.getRole() !== Role.tutee || this.state.postSucceeded){
            return <Redirect to={'/'}/>
        }
        return(
        <React.Fragment>
            <Container maxWidth="sm" style={this.styleReviewForm}>
                <h1 style={this.styleHeaders}><FormattedMessage id={"ReviewForm.header"}/></h1>
                <Divider variant="middle"/>
                <div style={this.styleForms}>
                    <InputLabel style={this.styleInputLabel}><FormattedMessage id={"ReviewForm.explanation"}/></InputLabel>
                    <TextField
                        style={this.styleTextField}
                        value={this.state.explanationValue}
                        name="explanation"
                        margin="normal"
                        multiline
                        rows="5"
                        rowsMax="5"
                        error={this.state.explanationIncorrect}
                        helperText={this.renderHelperText(this.state.explanationIncorrect)}
                        fullWidth={true}
                        variant="outlined"
                        onChange={this.onChange}
                    />
                    <InputLabel style={this.styleInputLabel}><FormattedMessage id={"ReviewForm.communication"}/></InputLabel>
                    <TextField
                        style={this.styleTextField}
                        value={this.state.communicationValue}
                        name="communication"
                        margin="normal"
                        multiline
                        rows="5"
                        rowsMax="5"
                        error={this.state.communicationIncorrect}
                        helperText={this.renderHelperText(this.state.communicationIncorrect)}
                        fullWidth={true}
                        variant="outlined"
                        onChange={this.onChange}
                    />
                    <InputLabel style={this.styleInputLabel}><FormattedMessage id={"ReviewForm.organisation"}/></InputLabel>
                    <TextField
                        style={this.styleTextField}
                        value={this.state.organisationValue}
                        name="organisation"
                        margin="normal"
                        multiline
                        rows="5"
                        rowsMax="5"
                        error={this.state.organisationIncorrect}
                        helperText={this.renderHelperText(this.state.organisationIncorrect)}
                        fullWidth={true}
                        variant="outlined"
                        onChange={this.onChange}
                    />
                    <InputLabel style={this.styleInputLabel}><FormattedMessage id={"ReviewForm.extra"}/></InputLabel>
                    <TextField
                        style={this.styleTextField}
                        value={this.state.extraValue}
                        name="extra"
                        margin="normal"
                        multiline
                        rows="5"
                        rowsMax="5"
                        fullWidth={true}
                        variant="outlined"
                        onChange={this.onChange}
                    />
                </div>
                <Button
                    variant="contained"
                    color="primary"
                    style={this.styleButton}
                    onClick={this.postReview}
                >
                    <FormattedMessage id={"ReviewForm.post"}/>
                </Button>
            </Container>
        </React.Fragment>
        );
    }

}
