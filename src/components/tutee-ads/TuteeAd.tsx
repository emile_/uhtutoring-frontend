import React, {Component} from 'react'
import "./TuteeAd.css"
import {Grid, Paper, Button, Typography} from "@material-ui/core";
import axios from 'axios';
import {authenticationService} from "../../_services/authenticationService";
import {FormattedMessage} from "react-intl";
import TuteeAdListItem from "./TuteeAdList";
import {Role} from "../../_helpers/role";

interface Props {
    tuteeAd: TuteeAdListItem
}
interface State {
    redirectToConversation: boolean,
    redirectConversationID:number,
    conversationPartnerName:string
}
/**
 * @description A form with a title and description field. Allows accepting the advertisement which creates a tutorship.
 * @author Koen laermans
 */
export default class TuteeAd extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            redirectToConversation:false,
            redirectConversationID:0,
            conversationPartnerName:""
        };

        this.onAccept = this.onAccept.bind(this);
    }
    /**
     * @description Handles the accepting of a tutorship with the tutee that posted the ad. Creates a new tutorship and conversation.
     */
    async onAccept() {
        axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();

        const TutorshipsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/tutorships",
            responseType : "json",
        });

        let tutorShipID:number = (await TutorshipsAPI.post("/", {
            tuteeID: this.props.tuteeAd.tuteeID,
            advertisementID: this.props.tuteeAd.id
        })).data.data.id;

        const MessageAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/messages",
            responseType : "json",
        });

        await MessageAPI.post("/conversation/"+tutorShipID, {});

        window.location.reload();
    }


    render() {

        return (
            <Paper className={"tuteeAdpaper"}>
                <Grid>
                    <Grid className={"tuteeAdGridItem title"} item xs={12}>
                        <Typography variant="h6" className={"title"}>
                            <FormattedMessage id="TuteeAd.Subject"/>{this.props.tuteeAd.title}
                        </Typography>
                    </Grid>
                    <Grid className={"tuteeAdGridItem date"} item xs={12}>
                        {new Date(this.props.tuteeAd.updateTimestamp).toLocaleDateString() +" "+new Date(this.props.tuteeAd.updateTimestamp).toLocaleTimeString()}
                    </Grid>
                    <Grid className={"tuteeAdGridItem"} item xs={12}>
                        {this.props.tuteeAd.description}
                    </Grid>
                    {authenticationService.getRole() === Role.tutor ? (
                        <Grid className={"tuteeAdGridItem buttons"} item xs={12}>
                            <Button className={"tuteeAdButton"} onClick={this.onAccept}><FormattedMessage id="TuteeAd.AcceptButton"/></Button>
                        </Grid>
                    ) : (
                        <Grid className={"tuteeAdGridItem buttons"} item xs={12}/>
                    )}
                </Grid>
            </Paper>

        );
    }
}
