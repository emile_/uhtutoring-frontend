import React, {Component, ReactElement} from 'react'
import "./TuteeAdList.css"
import {Grid} from "@material-ui/core";
import TuteeAd from "./TuteeAd";
import axios from 'axios';
import MyTuteeAd from "./MyTuteeAd";
import {Role} from "../../_helpers/role";
import {authenticationService} from '../../_services/authenticationService';
import ProgrammeSelection from "./ProgrammeSelection";
import ErrorSnackbar from "../ErrorHandlers/ErrorSnackbar";
import {FormattedMessage} from "react-intl";

interface Props {

}

interface State {
    tuteeAds : TuteeAdListItem[]
    selectedProgramme : string,
    role: null|Role,
    showErrorSnackbar : boolean
}

export default interface TuteeAdListItem {
    id:number,
    title: string,
    description: string,
    tuteeID:number,
    creationTimestamp: string,
    updateTimestamp: string,
    programme: string,
    accepted:boolean,
    acceptedDate:Date
}
/**
 * @description Shows all advertisements that a tutee posted in case a tutee is logged in. Else all advertisements of all tutees.
 * @author Koen Laermans
 */
export default class TuteeAdList extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            tuteeAds: [],
            selectedProgramme:authenticationService.getProgramme() as string,
            role:authenticationService.getRole(),
            showErrorSnackbar:false
        };
        this.handleSelectProgram = this.handleSelectProgram.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClose(){
        this.setState({
            showErrorSnackbar:false
        })
    }

    /**
     * @description Fetches all advertisements from the database
     */
    async componentDidMount() {
        axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();

        const TutorshipsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/tutorships",
            responseType : "json",
        });

        axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();

        const AdvertisementsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/applications",
            responseType : "json",
        });

        try{
            let tuteeAds:TuteeAdListItem[] = (await AdvertisementsAPI.get('/')).data.data;

            if (this.state.role === Role.tutee){
                let tutorShips = (await TutorshipsAPI.get('/')).data.data;
                let tutorShipAdvertisementIDs:number[] = [];
                for (let i=0;i<tutorShips.length;i++)
                    tutorShipAdvertisementIDs = tutorShipAdvertisementIDs.concat(tutorShips[i].advertisementID);

                for (let i=0;i<tuteeAds.length;i++){
                    tuteeAds[i].accepted = tutorShipAdvertisementIDs.includes(tuteeAds[i].id);
                }
            }
            tuteeAds.sort(function(a:TuteeAdListItem, b:TuteeAdListItem){return (a.updateTimestamp < b.updateTimestamp ? 0:-1)});

            this.setState({
                tuteeAds
            })
        }catch (e) {
            this.setState({
                showErrorSnackbar:true
            });
            return;
        }
    }

    /**
     * @description Returns the layout of an advertisement, based on the role of the person that is logged in.
     * @param tuteeAd The data that the layout must contain
     */
    getAdLayout(tuteeAd : TuteeAdListItem): ReactElement{
        switch (this.state.role) {
            case Role.tutee:
                return (<MyTuteeAd
                    key={tuteeAd.id}
                    tuteeAd={tuteeAd}
                />);
            default:
                return (<TuteeAd
                    key={tuteeAd.id}
                    tuteeAd={tuteeAd}
                />);
        }
    }

    /**
     * @description handles the selecting of a different program to sort the ads on
     * @param programme the name of the programme
     */
    handleSelectProgram(programme:string){
        if (programme!==this.state.selectedProgramme) {
            this.setState({
                selectedProgramme: programme
            })
        }
    }


    render() {
        console.log(this.state.tuteeAds)
        return (

            <Grid className={"tuteeAdList"} container spacing={3}>
                {authenticationService.getRole() !== Role.tutee && <ProgrammeSelection handleSelectProgram={this.handleSelectProgram} selectedProgramme={this.state.selectedProgramme}/>}
                {this.state.tuteeAds.map(
                    (tuteeAd:TuteeAdListItem) =>(
                        tuteeAd.programme === this.state.selectedProgramme &&
                        this.getAdLayout(tuteeAd)
                    )
                )}
                {this.state.tuteeAds.length === 0 && (
                    <Grid item lg={12}><FormattedMessage id={"tuteeAdList.noAds"}/>{this.state.role === Role.tutee && <FormattedMessage id={"tuteeAdList.createOne"}/>}</Grid>
                )}
                {(this.state.showErrorSnackbar && (
                    <FormattedMessage id={"tuteeAdList.error"}>
                        {translation =>
                            <ErrorSnackbar message={translation as string} handleClose={this.handleClose}/>
                        }
                    </FormattedMessage>))
                }
            </Grid>
        );
    }
}
