import React, { Component} from "react";
import axios from "axios";
import {authenticationService} from "../../_services/authenticationService";
import {
    FormControl,
    Grid, InputLabel,
    MenuItem,
    Select,
} from "@material-ui/core";
import {Programme} from "../Admin/AdminPanel";
import {FormattedMessage} from "react-intl";


interface Props {
    selectedProgramme:string
    handleSelectProgram:(programme:string) => void,
}

interface State {
    programmeSelectMap:{[value: number] : string},
    selectedProgrammeValue:number,
}

/**
 * @author koen laermans
 * @description shows a select dropdown to select a program
 */
export default class ProgrammeSelection extends Component<Props, State> {

    state:State;

    constructor (props: Props){
        super(props);
        this.state = {
            programmeSelectMap:{},
            selectedProgrammeValue:0,
        };
        this.onSelectProgramme = this.onSelectProgramme.bind(this);
        this.initProgrammeSelectMap = this.initProgrammeSelectMap.bind(this);
    }

    /**
     * @description puts initial values into the dropdown
     */
    initProgrammeSelectMap(programmes:Programme[]):{[value: number] : string}{
        let programmeSelectMap: {[value: number] : string} = {};
        for (let i=0;i<programmes.length;i++){
            programmeSelectMap[i]=programmes[i].name;
            if (programmes[i].name === this.props.selectedProgramme)
                this.setState({selectedProgrammeValue:i})
        }
        return programmeSelectMap
    }

    /**
     * @description handles selecting of a value of the dropdown
     * @param value: the index of the value in the list
     */
    onSelectProgramme(value:number){
        this.setState({
            selectedProgrammeValue:value
        });
        this.props.handleSelectProgram(this.state.programmeSelectMap[value])
    }

    /**
     * @description fetches all programmes from the database
     */
    async componentDidMount() {
        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const ProgrammesAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/programmes",
            responseType : "json",
        });


        let programmes:Programme[] = (await ProgrammesAPI.get("/")).data.data;

        this.setState({
            programmeSelectMap: this.initProgrammeSelectMap(programmes)
        });
    }


    render() {

        return (
            <Grid item className={"programmeSelection"}>
                <FormControl className={""}>
                    <InputLabel htmlFor="age-native-simple"><FormattedMessage id={"programmeSelection.label"}/></InputLabel>
                    <Select
                        name={"educationValue"}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={this.state.selectedProgrammeValue}
                        onChange={(e)=>this.onSelectProgramme(e.target.value as number)}

                    >
                        {Object.entries(this.state.programmeSelectMap)
                            .map( ([key, value]) =>(
                                <MenuItem key={key} value={key}>
                                    {value}
                                </MenuItem> ))
                        }
                    </Select>
                </FormControl>
            </Grid>
        );
    }
}
