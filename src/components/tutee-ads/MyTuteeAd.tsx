import React, {Component} from 'react'
import "./TuteeAd.css"
import {Grid, Paper, Button, Typography} from "@material-ui/core";
import axios from 'axios';
import {Redirect} from "react-router-dom";
import {authenticationService} from "../../_services/authenticationService";
import {FormattedMessage} from "react-intl";
import TuteeAdListItem from "./TuteeAdList";

interface Props {
    tuteeAd: TuteeAdListItem
}
interface State {
    redirectToEdit: boolean,
    isHidden: boolean
}

/**
 * @description A form with a title and description field. Allows deletion and edit of the advertisement.
 * @author Koen Laermans
 */
export default class MyTuteeAd extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            redirectToEdit:false,
            isHidden:false
        };

        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);

    }

    /**
     * @description Handles deletion of the advertisement
     */
    async onDelete(){

        axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();

        const AdvertisementsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/applications",
            responseType : "json",
        });

        await AdvertisementsAPI.delete("/"+this.props.tuteeAd.id, {});

        this.setState({
            isHidden:true
        })
    }

    /**
     * @description Handles editing of the advertisement. Redirects to edit form.
     */
    onEdit(): void {
        this.setState({
            redirectToEdit:true
        })
    }


    render() {

        if (this.state.redirectToEdit){
            return <Redirect to={{
                pathname: '/CreateAdvertisement',
                state: {
                    id: this.props.tuteeAd.id,
                    title: this.props.tuteeAd.title,
                    description: this.props.tuteeAd.description
                }
            }}
            />
        }

        return (

            (this.state.isHidden ? <React.Fragment/> : (<Paper className={"tuteeAdpaper "+(this.props.tuteeAd.accepted && "acceptedItem")}>
                    <Grid>
                        <Grid className={"tuteeAdGridItem title"} item xs={12}>
                            <Typography variant="h6" className={"title"}>
                                <FormattedMessage id="MyTuteeAd.subject"/>{this.props.tuteeAd.title + (this.props.tuteeAd.accepted ? " (Accepted)" : "")}
                            </Typography>
                        </Grid>
                        <Grid className={"tuteeAdGridItem date"} item xs={12}>
                            {new Date(this.props.tuteeAd.updateTimestamp).toLocaleDateString() +" "+new Date(this.props.tuteeAd.updateTimestamp).toLocaleTimeString()}
                        </Grid>
                        <Grid className={"tuteeAdGridItem"} item xs={12}>
                            {this.props.tuteeAd.description}
                        </Grid>
                        {!this.props.tuteeAd.accepted ? (<Grid className={"tuteeAdGridItem buttons"} item xs={12}>
                            <Button className={"tuteeAdButton"} onClick={this.onDelete}><FormattedMessage id="MyTuteeAd.deleteButton"/></Button>
                            <Button className={"tuteeAdButton"} onClick={this.onEdit}><FormattedMessage id="MyTuteeAd.editButton"/></Button>
                        </Grid>):(<Grid className={"tuteeAdGridItem buttons"} item xs={12}/>)}

                    </Grid>
                </Paper>))
        );
    }
}
