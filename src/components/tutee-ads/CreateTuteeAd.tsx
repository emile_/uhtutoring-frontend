import React, {Component} from 'react'
import "./TuteeAd.css"
import {Grid, Paper, Button, Typography, TextField} from "@material-ui/core";
import axios from 'axios';
import {Redirect} from "react-router-dom";
import {authenticationService} from "../../_services/authenticationService";
import {FormattedMessage} from "react-intl";
import ErrorSnackbar from "../ErrorHandlers/ErrorSnackbar";
import {Role} from "../../_helpers/role";

interface Props {
    location:{
        state: {
            title: string,
            description: string
            id: number
        }
    }
}
interface State {
    title:string,
    description: string,
    validTitle: boolean,
    validDescription: boolean,
    redirectToMyTuteeAds: boolean,
    showErrorSnackbar : boolean
}

/**
 * @description A form with a title and description field, to create a new advertisement
 * @author Koen Laermans
 */
export default class CreateTuteeAd extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            title: props.location.state.title,
            description: props.location.state.description,
            validTitle:true,
            validDescription:true,
            redirectToMyTuteeAds: false,
            showErrorSnackbar:false
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.isValidInput = this.isValidInput.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    /**
     * @description Checks is the title and description have a valid value
     */
    isValidInput():boolean{
        let titleLength:number = this.state.title.length;
        let descriptionLength:number = this.state.description.length;

        let validTitle:boolean = true;
        let validDescription:boolean = true;

        if (titleLength < 5 || titleLength > 70){
            validTitle = false;
        }
        if (descriptionLength < 20 || descriptionLength > 500){
            validDescription = false;
        }

        this.setState({
            validTitle,
            validDescription,
        });
        return (validTitle && validDescription);
    }


    handleClose(){
        this.setState({
            showErrorSnackbar:false
        })
    }


    /**
     * @description Handles cancellation of advertisement creation
     */
    onCancel():void{
        this.setState({
            redirectToMyTuteeAds:true
        });
    }

    /**
     * @description Handles the submission of a advertisement. Stores it in database.
     */
    async onSubmit(){

        if (!this.isValidInput())
            return;

        axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();

        const AdvertisementsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/applications",
            responseType : "json",
        });

        try {
            if (this.props.location.state.id === -1) {
                await AdvertisementsAPI.post("/", {
                    title: this.state.title,
                    description: this.state.description
                });
            } else {
                await AdvertisementsAPI.put("/" + this.props.location.state.id, {
                    title: this.state.title,
                    description: this.state.description
                });
            }
        }catch (e) {
            this.setState({
                showErrorSnackbar:true
            });
            return;
        }
        this.setState({
            redirectToMyTuteeAds:true
        });
    }


    render() {

        if (this.state.redirectToMyTuteeAds) {
            return <Redirect to='/Advertisements'/>;
        }

        if(authenticationService.getRole() !== Role.tutee){
            return <Redirect to={'/'}/>
        }

        return (
            <div className={"container"}>
                <Paper className={"createTuteeAdpaper"}>
                    <Grid>
                        <Grid className={"tuteeAdGridItem"} item xs={12}>
                            <Typography variant="h6">
                                {this.props.location.state.id===-1? <FormattedMessage id="CreateTuteeAd.submitMessage1"/>:<FormattedMessage id="CreateTuteeAd.submitMessage2"/>}
                            </Typography>
                        </Grid>
                        <Grid className={"createTuteeAdGridItem  title"} item >
                            <TextField
                                className={"textField"}
                                label={<FormattedMessage id="CreateTuteeAd.titleLabel"/>}
                                margin="normal"
                                variant="outlined"
                                value={this.state.title}
                                error={!this.state.validTitle}
                                helperText={!this.state.validTitle && <FormattedMessage id="CreateTuteeAd.titleError"/>}
                                onChange={(e) => this.setState({title: e.target.value})}
                            />
                        </Grid>
                        <Grid className={"createTuteeAdGridItem title"} item >
                            <TextField
                                className={"textField"}
                                label={<FormattedMessage id="CreateTuteeAd.descriptionLabel"/>}
                                margin="normal"
                                variant="outlined"
                                multiline
                                rows={5}
                                value={this.state.description}
                                error={!this.state.validDescription}
                                helperText={!this.state.validDescription && <FormattedMessage id="CreateTuteeAd.descriptionError"/>}
                                onChange={(e) => this.setState({description: e.target.value})}

                            />
                        </Grid>
                        <Grid className={"createTuteeAdGridItem buttons"} item >
                            <Button className={"tuteeAdButton"} onClick={this.onSubmit}><FormattedMessage id="CreateTuteeAd.submitButton"/></Button>
                            <Button className={"tuteeAdButton"} onClick={this.onCancel}><FormattedMessage id="CreateTuteeAd.cancelButton"/></Button>
                        </Grid>
                    </Grid>
                </Paper>
                {(this.state.showErrorSnackbar && (
                    <FormattedMessage id={"createTuteeAd.error"}>
                        {translation=>
                            <ErrorSnackbar message={translation as string} handleClose={this.handleClose}/>
                        }
                    </FormattedMessage>)
                )}
            </div>

        );
    }
};
