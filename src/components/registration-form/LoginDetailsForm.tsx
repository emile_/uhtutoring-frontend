import React, {Component, CSSProperties, FormEvent} from "react";
import {createMuiTheme, IconButton, InputLabel, TextField} from "@material-ui/core";
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {EmailIncorrectType, PasswordIncorrectType, PasswordRepeatIncorrectType} from "./RegistrationForm";
import {FormattedMessage} from "react-intl";

/**
 * State
 */
interface IState {
    showPassword : boolean
    showPasswordRepeat : boolean
}

/**
 * Props
 */
interface IProps {
    emailValue : string
    emailIncorrect : boolean
    emailIncorrectType : EmailIncorrectType
    passwordValue : string
    passwordIncorrect : boolean
    passwordIncorrectType : PasswordIncorrectType
    passwordRepeatValue : string
    passwordRepeatIncorrect : boolean
    passwordRepeatIncorrectType : PasswordRepeatIncorrectType
    onChange(event : FormEvent<HTMLInputElement | HTMLTextAreaElement>) : void
}

/**
 * LoginDetailsForm component
 */
export default class LoginDetailsForm extends Component<IProps,IState>{
    /**
     * Constructor
     */
    constructor(props : IProps){
        super(props);
        this.state={
            showPassword : false,
            showPasswordRepeat : false
        };
    }

    // Create a theme
    theme = createMuiTheme();

    /**
     * All styling properties
     */
    styleTextField : CSSProperties = {
        marginLeft: this.theme.spacing(1),
        marginRight: this.theme.spacing(1),
        width: 400,
    };

    styleInputLabel : CSSProperties = {
        paddingTop: this.theme.spacing(2),
        paddingBottom: this.theme.spacing(0.2),
    };

    /**
     * Toggle visibility of the password field
     */
    handleClickShowPassword = () => {
            this.setState({showPassword : !this.state.showPassword});
    };

    /**
     * Toggle visibility of the repeat password field
     */
    handleClickShowPasswordRepeat = () => {
        this.setState({showPasswordRepeat : !this.state.showPasswordRepeat});
    };

    /**
     * Returns correct helper text for the email field.
     */
    helperTextEmailInput(){

        // Something is incorrect.
        if (this.props.emailIncorrect){

            // The field is empty.
            if (this.props.emailIncorrectType === EmailIncorrectType.emptyField){
                return <FormattedMessage id="LoginDetailsForm.emptyField"/>
            }

            // The input has an incorrect format.
            else if (this.props.emailIncorrectType === EmailIncorrectType.invalidFormat){
                return <FormattedMessage id="LoginDetailsForm.emailIncorrectFormat"/>
            }

            // The input is already in use.
            else {
                return <FormattedMessage id="LoginDetailsForm.emailAlreadyInUse"/>
            }
        }

        // Everything is alright.
        else {
            return "";
        }

    }

    /**
     * Returns correct helper text for the password field.
     */
    helperTextPasswordInput(){

        // Something is incorrect.
        if (this.props.passwordIncorrect){

            // The field is empty.
            if (this.props.passwordIncorrectType === PasswordIncorrectType.emptyField){
                return <FormattedMessage id="LoginDetailsForm.emptyField"/>
            }

            // The input has an incorrect format.
            else if (this.props.passwordIncorrectType === PasswordIncorrectType.invalidFormat){
                return <FormattedMessage id="LoginDetailsForm.invalidPasswordFormat"/>
            }
        }

        // Everything is alright.
        else {
            return "";
        }
    }

    /**
     * Returns correct helper text for the repeat password field.
     */
    helperTextPasswordRepeatInput(){

        // Something is incorrect.
        if (this.props.passwordRepeatIncorrect){

            // The field is empty.
            if (this.props.passwordRepeatIncorrectType === PasswordRepeatIncorrectType.emptyField){
                return <FormattedMessage id="LoginDetailsForm.emptyField"/>
            }

            // The passwords do not match.
            else if (this.props.passwordRepeatIncorrectType === PasswordRepeatIncorrectType.doesNotMatch){
                return <FormattedMessage id="LoginDetailsForm.passwordsDoNotMatch"/>
            }
        }
        // Everything is alright.
        else {
            return "";
        }
    }

    /**
     * Render
     */
    render() {
        return (
            <div className="LoginDetailsForm">
                <InputLabel style={this.styleInputLabel}><FormattedMessage id="LoginDetailsForm.emailField"/></InputLabel>
                <TextField
                    style={this.styleTextField}
                    name="email"
                    margin="normal"
                    helperText={this.helperTextEmailInput()}
                    error={this.props.emailIncorrect}
                    value={this.props.emailValue}
                    onChange={this.props.onChange}
                    fullWidth={true}
                    variant="outlined"
                />
                <InputLabel style={this.styleInputLabel}><FormattedMessage id="LoginDetailsForm.passwordField"/></InputLabel>
                <TextField
                    style={this.styleTextField}
                    name="password"
                    helperText={this.helperTextPasswordInput()}
                    error={this.props.passwordIncorrect}
                    value={this.props.passwordValue}
                    onChange={this.props.onChange}
                    type={this.state.showPassword ? 'text' : 'password'}
                    margin="normal"
                    fullWidth={true}
                    variant="outlined"
                    InputProps={
                        {
                        endAdornment : <InputAdornment position="end">
                            <IconButton
                                onClick={this.handleClickShowPassword}
                            >
                                {this.state.showPassword ? <VisibilityOff/> : <Visibility/>}
                            </IconButton>
                        </InputAdornment>
                        }
                    }
                />
                <InputLabel style={this.styleInputLabel}><FormattedMessage id="LoginDetailsForm.passwordRepeatField"/></InputLabel>
                <TextField
                    style={this.styleTextField}
                    name="passwordRepeat"
                    helperText={this.helperTextPasswordRepeatInput()}
                    error={this.props.passwordRepeatIncorrect}
                    value={this.props.passwordRepeatValue}
                    onChange={this.props.onChange}
                    type={this.state.showPasswordRepeat ? 'text' : 'password'}
                    margin="normal"
                    fullWidth={true}
                    variant="outlined"
                    InputProps={
                        {
                            endAdornment : <InputAdornment position="end">
                                <IconButton
                                    onClick={this.handleClickShowPasswordRepeat}
                                >
                                    {this.state.showPasswordRepeat ? <VisibilityOff/> : <Visibility/>}
                                </IconButton>
                            </InputAdornment>
                        }
                    }
                />
            </div>
            );
        }

}

