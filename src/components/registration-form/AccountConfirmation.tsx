import React, {Component, CSSProperties} from 'react'
import check from './images/green_check.png'
import {createMuiTheme} from "@material-ui/core";
import {FormattedMessage} from "react-intl";

/**
 * AccountConfirmation component
 */
export default class  AccountConfirmation extends Component {

    // Create a theme
    theme = createMuiTheme();

    /**
     * All styling properties
     */
    styleConfirmation : CSSProperties = {
        alignContent: 'center',
        textAlign : 'center',
        paddingTop: this.theme.spacing(3),
        paddingBottom: this.theme.spacing(3),
    };

    /**
     * Render
     */
    render(){
        return(
            <div className="AccountConfirmation" style={this.styleConfirmation}>
                <img src={check} width={175} alt={"green check."}/><br/>
                <p><FormattedMessage id="AccountConfirmation.text"/></p>
            </div>
        );
    }
}
