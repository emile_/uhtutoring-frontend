import React, {ChangeEvent, Component, CSSProperties, FormEvent} from 'react';
import {createMuiTheme} from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import PersonalInformationForm from './PersonalInformationForm';
import LoginDetailsForm from './LoginDetailsForm'
import AccountConfirmation from './AccountConfirmation'
import Divider from '@material-ui/core/Divider';
import {FormattedMessage} from 'react-intl';
import Container from '@material-ui/core/Container';
import axios, {AxiosError, AxiosResponse} from 'axios';
import {Redirect} from 'react-router-dom';
import {authenticationService} from "../../_services/authenticationService";
import {Role, roleToString} from "../../_helpers/role";
import {Programme} from "../../_helpers/backend-data";

/**
 * Email incorrect types enum
 */
export enum EmailIncorrectType {
    emptyField,
    invalidFormat,
    alreadyInUse,
}

/**
 * Password incorrect types enum
 */
export enum PasswordIncorrectType {
    emptyField,
    invalidFormat,
}

/**
 * Password repeat incorrect types enum
 */
export enum PasswordRepeatIncorrectType {
    emptyField,
    doesNotMatch
}

/**
 * Props
 */
interface IProps {}

/**
 * State
 */
interface IState {
    activeStep: number;
    firstNameValue : string
    firstNameIncorrect : boolean
    lastNameValue : string
    lastNameIncorrect : boolean
    programmeValue : string
    programmeIncorrect : boolean
    availableProgrammes : Array<String>
    tutorMotivationValue : string
    tutorMotivationIncorrect : boolean
    emailValue : string
    emailIncorrect : boolean
    emailIncorrectType : EmailIncorrectType
    passwordValue : string
    passwordIncorrect : boolean
    passwordIncorrectType : PasswordIncorrectType
    passwordRepeatValue : string
    passwordRepeatIncorrect : boolean
    passwordRepeatIncorrectType : PasswordRepeatIncorrectType
    maxLengthEmail : number
    maxLengthName : number
    maxLengthPassword : number
    maxLengthMotivation : number
    accountTypeValue : Role
    postSucceeded : boolean
    loggedIn : boolean
}

/**
 * RegistrationForm component
 */
export default class RegistrationForm extends Component<IProps, IState> {

    /**
     * Constructor
     */
    constructor(props : IProps) {
        super(props);
        this.state = {
            activeStep : 0,
            firstNameValue : "",
            firstNameIncorrect : false,
            lastNameValue : "",
            lastNameIncorrect : false,
            programmeValue : "",
            programmeIncorrect: false,
            availableProgrammes : new Array<String>(),
            tutorMotivationValue : "",
            tutorMotivationIncorrect : false,
            emailValue : "",
            emailIncorrect : false,
            emailIncorrectType : EmailIncorrectType.emptyField,
            passwordValue : "",
            passwordIncorrect : false,
            passwordIncorrectType : PasswordIncorrectType.emptyField,
            passwordRepeatValue : "",
            passwordRepeatIncorrect : false,
            passwordRepeatIncorrectType : PasswordRepeatIncorrectType.emptyField,
            maxLengthEmail : 70,
            maxLengthName : 35,
            maxLengthPassword: 35,
            maxLengthMotivation : 200,
            accountTypeValue : Role.tutee,
            postSucceeded : false,
            loggedIn : authenticationService.getToken()!=null,
        };

        this.onChange = this.onChange.bind(this);
        this.setValue = this.setValue.bind(this);
        this.postAccount = this.postAccount.bind(this);
        this.handleResponse = this.handleResponse.bind(this);
        this.retrieveAvailableProgrammes = this.retrieveAvailableProgrammes.bind(this);
        this.onChangeProgramme = this.onChangeProgramme.bind(this);

        this.retrieveAvailableProgrammes();
    }

    // Create a theme
    theme = createMuiTheme();

    /**
     * All styling properties
     */
    styleRegistrationForm : CSSProperties = {
        boxShadow: '1px 3px 2px -2px rgba(0,0,0,0.2), 1px 2px 2px 1px rgba(0,0,0,0.14), 1px 2px 4px 1px rgba(0,0,0,0.12)'
    };

    styleButton : CSSProperties = {
        marginTop: this.theme.spacing(2),
        marginBottom: this.theme.spacing(2),
        marginLeft: this.theme.spacing(1),
    };

    styleForms: CSSProperties = {
        textAlign: 'left',
        marginTop: this.theme.spacing(2),
        marginLeft: this.theme.spacing(4),
        marginBottom: this.theme.spacing(1),
    };
    styleHeaders: CSSProperties = {
        paddingTop: this.theme.spacing(2),
        textAlign : 'center',
    };
    styleStepper: CSSProperties = {
        background: 'rgba(255, 255, 255, 0) ',
    };

    /**
     * Retrieve the available programmes
     */
    async retrieveAvailableProgrammes(){
        
        const ProgrammesAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/programmes",
            responseType : "json",
        });

        let availableProgrammes : Array<Programme> = (await ProgrammesAPI.get("/")).data.data;

        let programmesParsed = new Array<String>();
        for (let programme of availableProgrammes){
            programmesParsed.push(String(programme.name));
        }

        this.setState({availableProgrammes : programmesParsed});
    }

    /**
     * Gets array of available step headers.
     */
    getSteps() {
        return [ <FormattedMessage key="stepOne" id="RegistrationForm.personalInformationStep"/>,
            <FormattedMessage key="stepTwo" id="RegistrationForm.loginDetailsStep"/>
        ,<FormattedMessage key="stepThree" id="RegistrationForm.accountConfirmationStep"/>];
    }

    /**
     * Gets the content of a specific step index.
     */
    getStepContent(stepIndex: number) {
        switch (stepIndex) {
            case 0:
                return <PersonalInformationForm firstNameValue={this.state.firstNameValue} firstNameIncorrect={this.state.firstNameIncorrect} lastNameValue={this.state.lastNameValue} lastNameIncorrect={this.state.lastNameIncorrect} accountTypeValue={this.state.accountTypeValue} programmeIncorrect={this.state.programmeIncorrect} tutorMotivationValue={this.state.tutorMotivationValue} tutorMotivationIncorrect={this.state.tutorMotivationIncorrect} programmeValue={this.state.programmeValue} onChange={this.onChange} onChangeProgramme={this.onChangeProgramme}  availableProgrammes={this.state.availableProgrammes} onChangeAccountType={this.onChangeAccountType}/>;
            case 1:
                return <LoginDetailsForm emailValue={this.state.emailValue} emailIncorrect={this.state.emailIncorrect} emailIncorrectType={this.state.emailIncorrectType} passwordValue={this.state.passwordValue} passwordIncorrect={this.state.passwordIncorrect} passwordIncorrectType={this.state.passwordIncorrectType} passwordRepeatValue={this.state.passwordRepeatValue} passwordRepeatIncorrect={this.state.passwordRepeatIncorrect} passwordRepeatIncorrectType={this.state.passwordRepeatIncorrectType} onChange={this.onChange}/>;
            default:
                return 'Unknown stepIndex';
        }
    }

    /**
     * Goes to the next step if everything is alright.
     */
    handleNext = () => {

        let currentState : number = this.state.activeStep;
        let invalidField : boolean = false;

        switch (currentState) {
            case 0:
                // First name field is empty.
                if(this.state.firstNameValue.length === 0){
                    this.setState({firstNameIncorrect : true});
                    invalidField = true;
                }
                // Last name field is empty.
                if(this.state.lastNameValue.length === 0){
                    this.setState({lastNameIncorrect : true});
                    invalidField = true;
                }
                // Last name field is empty.
                if(this.state.accountTypeValue === Role.tutor && this.state.tutorMotivationValue.length === 0){
                    this.setState({tutorMotivationIncorrect : true});
                    invalidField = true;
                }

                // No programme selected.
                if (this.state.programmeValue === ""){
                    this.setState({programmeIncorrect : true});
                    invalidField = true;
                }

                break;

            case 1:
                // Email field does not have the UHasselt-Student format.
                if(!this.state.emailValue.endsWith("@student.uhasselt.be")){
                    this.setState({emailIncorrect : true});
                    this.setState({emailIncorrectType : EmailIncorrectType.invalidFormat});
                    invalidField = true;
                }
                // Email field is empty.
                if(this.state.emailValue.length === 0){
                    this.setState({emailIncorrect : true});
                    this.setState({emailIncorrectType : EmailIncorrectType.emptyField});
                    invalidField = true;
                }
                // Password field is not at least 5 characters long.
                if(this.state.passwordValue.length < 5){
                    this.setState({passwordIncorrect : true});
                    this.setState({passwordIncorrectType : PasswordIncorrectType.invalidFormat});
                    invalidField = true;
                }
                // Password field is empty.
                if(this.state.passwordValue.length === 0){
                    this.setState({passwordIncorrect : true});
                    this.setState({passwordIncorrectType : PasswordIncorrectType.emptyField});
                    invalidField = true;
                }
                // Password field and password repeat do not match.
                if(this.state.passwordRepeatValue !== this.state.passwordValue) {
                    this.setState({passwordRepeatIncorrect: true});
                    this.setState({passwordRepeatIncorrectType: PasswordRepeatIncorrectType.doesNotMatch});
                    invalidField = true;
                }
                // Password repeat field is empty.
                if (this.state.passwordRepeatValue.length === 0) {
                    this.setState({passwordRepeatIncorrect: true});
                    this.setState({passwordRepeatIncorrectType: PasswordRepeatIncorrectType.emptyField});
                    invalidField = true;
                }

                break;

            default:
                return ;
        }

        // Do not go to the next step because a field is invalid.
        if (invalidField) {
            return;
        }

        // Try to post the registration.
        if (this.state.activeStep === 1){

            // Post registration
            this.postAccount();
            this.setState({activeStep : currentState + 2});
            return;
        }

        // Go to the next step.
        this.setState({activeStep : currentState + 1});
    };

    /**
     * Go back to a previous step.
     */
    handleBack = () => {
        let currentStep : number = this.state.activeStep;

        // Reset all login details fields.
        if (currentStep === 1){
            // Reset the email field.
            this.setState({emailIncorrect : false});
            this.setState({emailValue : ""});

            // Reset the password field.
            this.setState({passwordIncorrect : false});
            this.setState({passwordValue : ""});

            // Reset the password repeat field.
            this.setState({passwordRepeatIncorrect : false});
            this.setState({passwordRepeatValue : ""});
        }

        // Go back one step.
        this.setState({activeStep : currentStep - 1})
    };

    /**
     * Update the value of a field.
     * @param field target field
     * @param value target value
     */
    setValue = (field : string, value : string) => {
        switch (field)
        {
            case 'firstName':
                // Max length for a name reached.
                if(value.length > this.state.maxLengthName){
                    return
                }
                this.setState({firstNameIncorrect : false});
                this.setState({firstNameValue : value});
                break;

            case 'lastName':
                // Max length for a name reached.
                if(value.length > this.state.maxLengthName){
                    return
                }
                this.setState({lastNameIncorrect : false});
                this.setState({lastNameValue : value});
                break;

            case 'tutorMotivation':
                // Max length for a motivation reached.
                if(value.length > this.state.maxLengthMotivation){
                    return
                }
                this.setState({tutorMotivationIncorrect : false});
                this.setState({tutorMotivationValue : value});
                break;

            case 'email':
                // Max length for an email address reached.
                if(value.length > this.state.maxLengthEmail){
                    return
                }
                this.setState({emailIncorrect : false});
                this.setState({emailValue : value});
                break;

            case 'password':
                // Max length for a password reached.
                if(value.length > this.state.maxLengthPassword){
                    return
                }

                // Reset passwords do not match incorrect type
                if(this.state.passwordRepeatIncorrect && this.state.passwordRepeatIncorrectType === PasswordRepeatIncorrectType.doesNotMatch){
                    this.setState({passwordRepeatIncorrect : false});
                    this.setState({passwordRepeatIncorrectType : PasswordRepeatIncorrectType.emptyField});
                }

                this.setState({passwordIncorrect : false});
                this.setState({passwordValue : value});
                break;

            case 'passwordRepeat':
                // Max length for a password reached.
                if(value.length > this.state.maxLengthPassword){
                    return
                }
                this.setState({passwordRepeatIncorrect : false});
                this.setState({passwordRepeatValue : value});
                break;

            default:
                break;
        }
    };

    handleResponse = (response : AxiosResponse<any> | undefined ) : void => {


        // Response is not undefined.
        if (response) {

            // Response status code
            const statusCode: number = response.status;

            // Post succeeded.
            if (statusCode === 201) {
               this.setState({postSucceeded: true});
            }

            // Email is already in use.
            else if (statusCode === 400) {
                this.setState({emailIncorrect: true});
                this.setState({emailIncorrectType: EmailIncorrectType.alreadyInUse});
            }

        }
    };

    /**
     * Post the account details to the back end and create a new account.
     */
    postAccount = () => {

        /**
         * Account details
         */
        const firstName : string = this.state.firstNameValue;
        const lastName : string = this.state.lastNameValue;
        const email : string = this.state.emailValue;
        const password : string = this.state.passwordValue;
        const accountType : string = roleToString(this.state.accountTypeValue);
        const programme : string = this.state.programmeValue;

        let motivation : string;

        // Grab tutor motivation
        if (this.state.accountTypeValue === Role.tutor){
            motivation = this.state.tutorMotivationValue;
        }
        // No motivation necessary
        else {
           motivation = "";
        }


        // Post to the backend
        axios.post(process.env.REACT_APP_BACKEND_API_URL + '/users/register',{ 'firstname' : firstName, 'lastname' : lastName, 'email' : email, 'password' : password,'role' : accountType, 'motivation' : motivation, 'programme' : programme}).then(
            (response : AxiosResponse<any>) => {
                this.handleResponse(response);
            }
        ).catch(
            (error : AxiosError) => {
                this.handleResponse(error.response);
            }
        );
    };

    /**
     * When the input of a field has changed.
     * @param event event from input field
     */
    onChange = (event : FormEvent<HTMLInputElement>) =>{
        let targetField : string = event.currentTarget.name;
        let targetValue : string = event.currentTarget.value;
        this.setValue(targetField,targetValue);
    };

    /**
     * When the input of the account type select has changed.
     */
    onChangeAccountType = (event : ChangeEvent<{ name?: string | undefined; value: unknown; }>) =>{
        let targetValue  = event.target.value;

        // Reset tutor motivation
        this.setState({tutorMotivationValue : ""});

        // Set account type to tutee
        if (targetValue === 0){
            this.setState({accountTypeValue : Role.tutee});
        }

        // Set account type to tutor
        else if (targetValue === 1){
            this.setState({accountTypeValue : Role.tutor});
        }

        // Default set
        else{
            this.setState({accountTypeValue : Role.tutee});
        }
    };

    /**
     * When the input of the programme type select has changed.
     */
    onChangeProgramme = (programmeValue : string) =>{

        if (programmeValue){
            this.setState({programmeValue : programmeValue})
        }

        // Default set
        else{
            this.setState({programmeValue : ""});
        }

        this.setState({programmeIncorrect : false});
    };


    /**
     * Render
     */
    render() {

        // Redirect if logged in
        if(this.state.loggedIn){
            return <Redirect to={'/'}/>
        }

        let steps = this.getSteps();

        return (
            <React.Fragment>
                <Container maxWidth="sm">
                    <Stepper activeStep={this.state.activeStep} alternativeLabel={true} style={this.styleStepper}>
                        {steps.map((label) => (
                            <Step key="">
                                <StepLabel>{label}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    <div style={this.styleRegistrationForm}>
                        <h1 style={this.styleHeaders}><FormattedMessage id={"RegistrationForm.header"}/></h1>
                        <Divider variant="middle"/>
                    {this.state.postSucceeded ?
                        <AccountConfirmation/>
                        :
                            <div style={this.styleForms}>
                                {this.getStepContent(this.state.activeStep)}
                                <Button
                                    variant="contained"
                                    disabled={this.state.activeStep === 0}
                                    onClick={this.handleBack}
                                    style={this.styleButton}
                                >
                                    <FormattedMessage id={"RegistrationForm.previousStep"}/>
                                </Button>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={this.handleNext}
                                    style={this.styleButton}
                                >
                                    {this.state.activeStep === steps.length - 2 ? <FormattedMessage id={"RegistrationForm.lastStep"}/>
                                    : <FormattedMessage id={"RegistrationForm.nextStep"}/>
                                    }
                                </Button>
                            </div>
                    }
                    </div>
                </Container>
            </React.Fragment>
        );
    }
}
