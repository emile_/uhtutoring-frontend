import React, {ChangeEvent, FormEvent} from "react";
import {FormattedMessage} from 'react-intl';
import MenuItem from '@material-ui/core/MenuItem';
import {InputLabel, Select, TextField} from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import {Role} from "../../_helpers/role";


/**
 * Styles
 */
const useStyles = makeStyles(theme => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 400,
    },
    inputLabel: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(0.2),
    },
    formControl: {
        marginLeft: theme.spacing(1),
        paddingTop: theme.spacing(2),
        minWidth: 150,
    },
    picker: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginTop : theme.spacing(2),
        width: 400,
    },
}));

/**
 * Props
 */
interface IProps{
    firstNameValue : string
    firstNameIncorrect : boolean
    lastNameValue : string
    lastNameIncorrect : boolean
    accountTypeValue : Role
    tutorMotivationValue : string
    tutorMotivationIncorrect : boolean
    availableProgrammes : Array<String>
    programmeValue : string
    programmeIncorrect : boolean
    onChange(event : FormEvent<HTMLInputElement | HTMLTextAreaElement>) : void
    onChangeAccountType(event : ChangeEvent<{ name?: string | undefined; value: unknown; }>) : void
    onChangeProgramme(programmeValue : string) : void
}

/**
 * Returns PersonalInformationForm component
 */
export default function PersonalInformationForm(props : IProps) {

    /**
     * Returns correct helper text for the input fields.
     * @param incorrectField input field has correct input or not
     */
    function helperTextInputField(incorrectField : boolean){

        // Input field is invalid.
        if (incorrectField){
            return <FormattedMessage id="PersonalInformationForm.emptyField"/>
        }

        // Everything is correct.
        else {
            return "";
        }
    }

    /**
     * Returns correct helper text for the programme field.
     */
    function helperTextProgramme(){

        // Input field is invalid.
        if (props.programmeIncorrect){
            return <FormattedMessage id="PersonalInformationForm.emptyField"/>
        }

        // Everything is correct.
        else {
            return "";
        }
    }

    // Const containing all styles.
    const classes = useStyles();

    /**
     * Display tutor motivation label
     */
    function displayTutorMotivationLabel() {
        if (props.accountTypeValue === Role.tutor){
            return <InputLabel className={classes.inputLabel}><FormattedMessage id="PersonalInformationForm.tutorMotivation"/></InputLabel>;
        }
        else {
            return;
        }
    }

    /**
     * Display tutor motivation field
     */
    function displayTutorMotivationField() {
        if (props.accountTypeValue === Role.tutor){
            return <TextField
                className={classes.textField}
                multiline
                rows="5"
                rowsMax="5"
                error={props.tutorMotivationIncorrect}
                value={props.tutorMotivationValue}
                helperText={helperTextInputField(props.tutorMotivationIncorrect)}
                name="tutorMotivation"
                onChange={props.onChange}
                margin="normal"
                fullWidth={true}
                variant="outlined"
            />;
        }
        else {
            return;
        }
    }

    function onChangeProgramme(event : ChangeEvent<{ name?: string | undefined; value: string; }>){
        props.onChangeProgramme(event.target.value);
    }

    function  renderProgrammeSelect() {
        const menuItems = [];
        for (let programme of props.availableProgrammes) {
            menuItems.push(<MenuItem value={programme.toString()} key={programme.toString()}>{programme}</MenuItem>);
        }

        return <TextField select  variant="outlined" value={props.programmeValue} onChange={onChangeProgramme} fullWidth={true}  className={classes.picker} helperText={helperTextProgramme()} error={props.programmeIncorrect}>
            {menuItems}
        </TextField>;
    }

    /**
     * Returns the component
     */
    return (
        <div className="PersonalInformationForm">
            <InputLabel className={classes.inputLabel}><FormattedMessage id="PersonalInformationForm.firstName"/></InputLabel>
            <TextField
                className={classes.textField}
                error={props.firstNameIncorrect}
                value={props.firstNameValue}
                helperText={helperTextInputField(props.firstNameIncorrect)}
                name="firstName"
                onChange={props.onChange}
                margin="normal"
                fullWidth={true}
                variant="outlined"
            />
            <InputLabel className={classes.inputLabel}><FormattedMessage id="PersonalInformationForm.lastName"/></InputLabel>
            <TextField
                className={classes.textField}
                error={props.lastNameIncorrect}
                value={props.lastNameValue}
                helperText={helperTextInputField(props.lastNameIncorrect)}
                name="lastName"
                onChange={props.onChange}
                margin="normal"
                fullWidth={true}
                variant="outlined"
            />
            <InputLabel className={classes.inputLabel}><FormattedMessage id="PersonalInformationForm.programme"/></InputLabel>
            {renderProgrammeSelect()}
            <InputLabel className={classes.inputLabel}><FormattedMessage id="PersonalInformationForm.accountType"/></InputLabel>
            <FormControl variant="outlined" className={classes.formControl}>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={props.accountTypeValue}
                    onChange={props.onChangeAccountType}
                >
                    <MenuItem value={Role.tutee}><FormattedMessage id="PersonalInformationForm.tutee"/></MenuItem>
                    <MenuItem value={Role.tutor}><FormattedMessage id="PersonalInformationForm.tutor"/></MenuItem>
                </Select>
            </FormControl>
            {displayTutorMotivationLabel()}
            {displayTutorMotivationField()}
        </div>
    );
}
