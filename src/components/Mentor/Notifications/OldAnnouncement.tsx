import React from 'react'

import {Grid, Paper, ListItem} from "@material-ui/core";
import {FormattedMessage} from "react-intl";

interface Props {
    id:number,
    creationDate: Date,
    content: string,
    receivers:string[]
}

/**
 * @description An old announcement
 * @author Koen laermans
 */
export default function OldAnnouncement (props:Props) {
    return (
        <Paper className={"oldAnnouncement"} elevation={2} >
            <Grid>

                <Grid item className={"oldAnnouncementContentTitle"}>
                    <FormattedMessage id="OldAnnouncement.receivers"/>
                </Grid>
                <Grid className={"oldAnnouncementContent"} item xs={12}>
                    {props.receivers.map(
                        (receiver:string) =>
                            receiver+", "
                    )
                    }
                </Grid>
                <ListItem divider/>
                <Grid item className={"oldAnnouncementContentTitle"}>
                    <FormattedMessage id="OldAnnouncement.content"/>
                </Grid>

                <Grid className={"oldAnnouncementContent"} item xs={12}>
                    {props.content}
                </Grid>
                <ListItem divider/>
                <Grid className={"oldAnnouncementDate"} item xs={12}>
                    {props.creationDate.toLocaleDateString()}
                </Grid>

            </Grid>
        </Paper>

    );
}
