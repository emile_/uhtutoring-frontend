import React, {Component} from 'react'
import {
    Button,
    TextField,
    FormControl,
    FormGroup,
    FormLabel, FormControlLabel, Checkbox, Dialog, DialogTitle, DialogContent, DialogActions
} from "@material-ui/core";
import axios from "axios";
import "./NewAnnouncement.css"
import {FormattedMessage} from "react-intl";
import {authenticationService} from "../../../_services/authenticationService";


interface Props {
    handleCancel:() => void
}

interface State {
    tutors:string[],
    receivers:{[receiver:string]:boolean},
    content:string,
    title:string,
    validContent:boolean,
    validTitle:boolean,
    validSelectedReceivers:boolean
}

/**
 * @description A panel for the mentor to send announcements
 * @author Koen Laermans
 */
export default class NewAnnouncement extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            tutors:["Bert swerts","Joffer Roba","Pieter Jan"],
            receivers:{"Tutor":false,"Tutee":false,"Mentor":false},
            content:"",
            title:"",
            validContent:true,
            validTitle:true,
            validSelectedReceivers:true
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.isValidInput = this.isValidInput.bind(this);
        this.onChangeSelectReceiver = this.onChangeSelectReceiver.bind(this);
    }


    /**
     * @description Checks if the content is valid
     */
    isValidInput():boolean{
        let titleLength:number = this.state.title.length;
        let contentLength:number = this.state.content.length;
        let selectedReceiverLength:number = 0;
        let receivers:{[receiver:string]:boolean} = this.state.receivers;
        let receiverKeys:string[] = Object.keys(receivers);

        for (let i=0;i<receiverKeys.length;i++){
            receivers[receiverKeys[i]] && (selectedReceiverLength+=1);
        }

        let validContent:boolean = !(contentLength < 5 || contentLength > 500);
        let validSelectedReceivers:boolean = !(selectedReceiverLength === 0 || selectedReceiverLength>Object.keys(this.state.receivers).length);
        let validTitle:boolean = (titleLength !== 0);

        this.setState({
            validContent,
            validSelectedReceivers,
            validTitle,
        });
        return (validContent && validSelectedReceivers && validTitle);
    }


    /**
     * @description toggle check on a checkbox of a certain receiver
     * @param receiver: the name of the receiver to toggle
     */
    onChangeSelectReceiver(receiver:string){
        let receivers:{[receiver:string]:boolean} = this.state.receivers;
        receivers[receiver] = !receivers[receiver];
        this.setState({
            receivers
        })
    }


    /**
     * @description Handles the submission of a advertisement. Stores it in database.
     */
    async onSubmit(){

        if (!this.isValidInput())
            return;

        let receivers:{[receiver:string]:boolean} = this.state.receivers;
        let receiversKeys :string[] = Object.keys(receivers);
        let selectedReceivers:string[] = [];
        for (let i=0;i<receiversKeys.length;i++){
            receivers[receiversKeys[i]] && (selectedReceivers = selectedReceivers.concat(receiversKeys[i]));
        }

        axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();
        const NotificationsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/notifications",
            responseType : "json",
        });

        await NotificationsAPI.post("/multi/", {
            roles:selectedReceivers,
            title:this.state.title,
            content:this.state.content,
	        url:"",
        });

        this.props.handleCancel();

    }


    render() {
        const receivers:{[receiver:string]:boolean} = this.state.receivers;

        return (
            <Dialog open={true} aria-labelledby="form-dialog-title " PaperProps={{style: { borderRadius: 2,width:"100%"}}} >
                <DialogTitle id="form-dialog-title">
                    <FormattedMessage id="OldAnnouncementList.new"/>
                </DialogTitle>
                <DialogContent>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">
                            <FormattedMessage id="NewAnnouncement.receiverroles"/>
                        </FormLabel>
                        <FormGroup aria-label="position" row>

                            {(Object.keys(receivers).map((receiver,index)=>(
                                <FormControlLabel
                                    key={index}
                                    value={receiver}
                                    control={<Checkbox color="primary" />}
                                    label={receiver}
                                    labelPlacement="end"
                                    checked={receivers[receiver]}
                                    onChange={()=>this.onChangeSelectReceiver(receiver)}
                                />)
                            ))}

                        </FormGroup>
                    </FormControl>
                        {(!this.state.validSelectedReceivers && <div className={"error"}>Please select one or more receivers.</div>)}
                    <FormattedMessage id={"NewAnnouncement.title"}>
                        {translatedtitle =>
                            <TextField
                                id="outlined-basic"
                                className={"textField"}
                                label={translatedtitle}
                                margin="normal"
                                variant="outlined"
                                multiline
                                rows={1}
                                value={this.state.title}
                                error={!this.state.validTitle}
                                helperText={!this.state.validTitle &&
                                <FormattedMessage id={"fieldError1"}/>}
                                onChange={(e) => this.setState({title: e.target.value})}
                            />
                        }
                    </FormattedMessage>
                    <FormattedMessage id={"NewAnnouncement.content"}>
                        {translatedcontent =>
                            <TextField
                                id="outlined-basic"
                                className={"textField"}
                                label={translatedcontent}
                                margin="normal"
                                variant="outlined"
                                multiline
                                rows={5}
                                value={this.state.content}
                                error={!this.state.validContent}
                                helperText={!this.state.validContent &&
                                <FormattedMessage id={"fieldError1"}/>}
                                onChange={(e) => this.setState({content: e.target.value})}
                            />
                        }
                    </FormattedMessage>
		</DialogContent>
		<DialogActions>
                    <Button  color="inherit" onClick={this.props.handleCancel} className={"tuteeAdButton buttons"}>Cancel</Button>
                    <Button className={"tuteeAdButton buttons"} onClick={this.onSubmit}><FormattedMessage id="CreateTuteeAd.submitButton"/></Button>
                </DialogActions>
            </Dialog>

        );
    }
}
