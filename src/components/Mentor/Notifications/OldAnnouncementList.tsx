import React, {Component} from 'react'
import {Button, Grid, Paper} from "@material-ui/core";
import {FormattedMessage} from "react-intl";
import OldAnnouncement from "./OldAnnouncement";
import "./OldAnnouncementList.css"
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import NewAnnouncement from "./NewAnnouncement";


interface State {
    announcements : Announcement[],
    showNewAnnouncement:boolean
}

interface Announcement {
    id:number,
    content: string,
    creationTimestamp: string,
    receivers:string[]
}

/**
 * @description Shows all announcements that have been posted earlier.
 * @author Koen Laermans
 */
export default class OldAnnouncementList extends Component<{}, State> {

    state: State;

    constructor(props: {}){
        super(props);

        this.state = {
            //Should initialized in componentDidMount, if there is data in the database.
            announcements: [
                {id:3, content:"Wie had er een frietje besteld?",creationTimestamp:"2019-11-17T20:10:16.000Z",receivers:["All mentors","All tutors","All tutees"]},
                {id:0, content:"De bitterballen zijn op",creationTimestamp:"2019-11-17T20:10:16.000Z",receivers:["All mentors"]},
            ],
            showNewAnnouncement:false
        };
        this.handleCancel = this.handleCancel.bind(this);
    }

    /**
     * @description Fetches all announcements from the database
     */
    async componentDidMount() {

        {/*axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();

        const AdvertisementsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/advertisements",
            responseType : "json",
        });

        let notifications:Notification[] = (await AdvertisementsAPI.get('/')).data.data;

        this.setState({
            notifications
        })*/}
    }


    handleCancel():void{
        this.setState({
            showNewAnnouncement:false
        })
    }

    
    render() {

        return (
            <React.Fragment>
                <Paper className={"announcementList"} elevation={3}>
                    <Grid  container spacing={3}>
                        <div className={"announcementListHeader"}>
                            <Grid item className={"announcementListTitle"}>
                                <FormattedMessage id="Navigations.announcements"/>
                            </Grid>
                            <Grid item className={"newAnnouncement"} >
                                <Button className={"newAnnouncementButton"} onClick={()=>this.setState({showNewAnnouncement:true})}><AddCircleOutlineIcon/>
                                    <FormattedMessage id="OldAnnouncementList.new"/>
                                </Button>
                            </Grid>

                        </div>
                        {this.state.announcements.map(
                            (announcement:Announcement) =>
                                <OldAnnouncement
                                    key={announcement.id}
                                    id={announcement.id}
                                    content={announcement.content}
                                    creationDate={new Date(announcement.creationTimestamp)}
                                    receivers={announcement.receivers}
                                />
                        )
                        }
                    </Grid>
                </Paper>
                {this.state.showNewAnnouncement && (
                    <NewAnnouncement
                        handleCancel={this.handleCancel}
                    />)
                }
            </React.Fragment>
        );
    }
}
