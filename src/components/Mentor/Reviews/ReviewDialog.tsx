import React, {ChangeEvent} from 'react';
import {createStyles, makeStyles, Theme, withStyles, WithStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import {InputLabel, TextField} from "@material-ui/core";
import {Review} from "../../../_helpers/backend-data";
import {FormattedMessage} from "react-intl";

/**
 * Props
 */
interface IProps{
    open : boolean
    handleClose() : void
    review : Review
}

const useStyles = makeStyles(theme => ({
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 400,
        color: "black"
    },
    textFieldDisabled: {
        color: "black"
    },
    inputLabel: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(0.2),
        color: "black"
    },
}));

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}


const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default function ReviewDialog(props : IProps) {

    // Const containing all styles.
    const classes = useStyles();

    return (
        <Dialog onClose={props.handleClose} aria-labelledby="customized-dialog-title" open={props.open}>
            <DialogTitle id="customized-dialog-title" onClose={props.handleClose}>
                <FormattedMessage id={"ReviewDialog.header"}/>
            </DialogTitle>
            <DialogContent dividers>
                <InputLabel className={classes.inputLabel}><FormattedMessage id={"ReviewDialog.explanation"}/></InputLabel>
                <TextField
                    className={classes.textField}
                    name="subject"
                    margin="normal"
                    multiline
                    rows="5"
                    rowsMax="5"
                    value={props.review.content}
                    fullWidth={true}
                    variant="outlined"
                    disabled={true}
                    InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
                />
                <InputLabel className={classes.inputLabel}><FormattedMessage id={"ReviewDialog.communication"}/></InputLabel>
                <TextField
                    className={classes.textField}
                    name="subject"
                    margin="normal"
                    multiline
                    rows="5"
                    rowsMax="5"
                    value={props.review.communication}
                    fullWidth={true}
                    variant="outlined"
                    disabled={true}
                    InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
                />
                <InputLabel className={classes.inputLabel}><FormattedMessage id={"ReviewDialog.organisation"}/></InputLabel>
                <TextField
                    className={classes.textField}
                    name="subject"
                    margin="normal"
                    multiline
                    rows="5"
                    rowsMax="5"
                    value={props.review.organization}
                    fullWidth={true}
                    variant="outlined"
                    disabled={true}
                    InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
                />
                <InputLabel className={classes.inputLabel}><FormattedMessage id={"ReviewDialog.extra"}/></InputLabel>
                <TextField
                    className={classes.textField}
                    name="subject"
                    margin="normal"
                    multiline
                    rows="5"
                    rowsMax="5"
                    value={props.review.extra}
                    fullWidth={true}
                    variant="outlined"
                    disabled={true}
                    InputProps={{ classes: { disabled: classes.textFieldDisabled } }}
                />
            </DialogContent>
        </Dialog>
    );
}
