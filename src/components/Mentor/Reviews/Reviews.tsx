import React,{Component} from "react";
import {FormattedMessage} from "react-intl";
import {
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper,
    Typography
} from "@material-ui/core";
import {authenticationService} from "../../../_services/authenticationService";
import InfoIcon from '@material-ui/icons/Info';
import { IconButton } from '@material-ui/core';
import axios from "axios";
import ReviewDialog from "./ReviewDialog";
import {Review} from "../../../_helpers/backend-data";
import {Role} from "../../../_helpers/role";
import {Redirect} from "react-router-dom";

interface IState {
    reviews : Array<Review>
    openReview : boolean
    selectedReview : number
}

interface IProps  {

}

export default class TutorShips extends Component<IProps,IState>{
    /**
     * Costructor
     * @param props
     */
    constructor(props : any) {
        super(props);
        this.state = {
            reviews : new Array<Review>(),
            openReview : false,
            selectedReview : -1
        };
        this.retrieveReviews  = this.retrieveReviews.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.retrieveReviews();

    }

    /**
     * Retrieve reviews
     */
    async retrieveReviews() {

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const ReviewAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/reviews",
            responseType : "json",
        });

        let reviews = (await ReviewAPI.get("/")).data.reviews;
        this.setState({reviews : reviews});
    }



    /**
     * Open review dialog
     */
    renderReviewDialog(){
        if(this.state.openReview){
            return <ReviewDialog open={this.state.openReview} review={this.state.reviews[this.state.selectedReview]} handleClose={this.handleClose}/>;
        }
    }

    /**
     * Close review dialog
     */
    handleClose(){
        this.setState({openReview : false});
    }

    /**
     * Open a review
     * @param reviewNumber
     */
    openReview(reviewNumber : number){
        this.setState({selectedReview : reviewNumber,openReview : true});
    }

    /**
     * Render
     */
    render(){
        if(authenticationService.getRole() !== Role.mentor){
            return <Redirect to={'/'}/>
        }
        return  <React.Fragment>
            <Paper className={"pendingTutorPaper"} elevation={4}>
                <List className={"pendingTutorList"}>
                    <ListItem divider className={"listTitle"}>
                        <Grid container>
                            <Grid item >
                                <FormattedMessage id={"Reviews.amountOfReviews"}>
                                    {text =>
                                        <ListItemText><Typography
                                            variant="h6">{text}{this.state.reviews.length}</Typography></ListItemText>
                                    }
                                </FormattedMessage>
                            </Grid>
                        </Grid>
                    </ListItem>
                    <ListItem divider >
                        <Grid container>
                            <Grid item xs={2} >
                                <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                    <FormattedMessage id="Reviews.tutor"/>
                                </Typography></ListItemText>
                            </Grid>
                            <Grid item xs={2}>
                                <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                    <FormattedMessage id="Reviews.date"/>
                                </Typography></ListItemText>
                            </Grid>
                            <Grid item xs={3} >
                                <ListItemText ><Typography variant="h6" className={"pendingTutorListHeaderItem actions"}>
                                    <FormattedMessage id="Reviews.actions"/>
                                </Typography></ListItemText>
                            </Grid>
                        </Grid>
                    </ListItem>

                    {this.state.reviews.map((review : Review, index) => (
                        <ListItem key={review.id} divider className={"pendingTutorListBodyItem"}>
                            <Grid container>
                                <Grid item xs={2}>
                                    <ListItemText><Typography >{review.tutorName}</Typography></ListItemText>
                                </Grid>
                                <Grid item xs={2}>
                                    <ListItemText><Typography >{new Date(review.timestamp).toLocaleDateString()}</Typography></ListItemText>
                                </Grid>
                                <Grid item xs={2}>
                                    <IconButton onClick={()=>this.openReview(index)}><InfoIcon/></IconButton>
                                </Grid>
                            </Grid>
                        </ListItem>
                    ))}
                </List>
            </Paper>
            {this.renderReviewDialog()}
        </React.Fragment>;
    }
}
