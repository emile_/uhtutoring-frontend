import React, {Component} from 'react'
import "../../tutee-ads/TuteeAd.css"
import {
    Button,
    TextField,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogContentText, DialogActions
} from "@material-ui/core";
import {FormattedMessage} from "react-intl";


interface Props {
    accepted:boolean,
    firstName:string,
    lastName:string,
    updateTutorAcceptance:(email:string) => void
    handleCancel:() => void
}
interface State {
    content: string,
    validContent: boolean,
}

/**
 * @author Koen Laermans
 * @description Shows a form with an editable content field
 */
export default class Email extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            content: "Geen boodschap toegevoegd.",
            validContent:true,
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.isValidInput = this.isValidInput.bind(this);
        this.getContent = this.getContent.bind(this);
    }


    /**
     * @description returns the title of the form
     * @param accepted: whether the tutor has been accepted or rejected
     */
    getContent(accepted:boolean){


    }


    /**
     * @description Checks if the description is a valid value
     */
    isValidInput(): boolean{
        let descriptionLength:number = this.state.content.length;
        let validContent:boolean = true;

        if (descriptionLength < 5 || descriptionLength > 500)
            validContent = false;

        this.setState({
            validContent,
        });
        return validContent;
    }


    /**
     * @description Submits the form data
     */
    async onSubmit(){
        if (!this.isValidInput())
            return;
        this.props.updateTutorAcceptance(this.state.content)
    }


    render() {

        return (
            <Dialog open={true} aria-labelledby="form-dialog-title " PaperProps={{style: { borderRadius: 2 }}} >
                <DialogTitle id="form-dialog-title">
                    {this.props.accepted?<FormattedMessage id={"email.accepting"}/>:<FormattedMessage id={"email.rejecting"}/>}
                    {this.props.firstName+" "+this.props.lastName}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        <FormattedMessage id={"email.title"}/>
                    </DialogContentText>
                    <TextField
                        id="outlined-basic"
                        className={"textField"}
                        label={"Email"}
                        margin="normal"
                        variant="outlined"
                        multiline
                        rows={5}
                        value={this.state.content}
                        error={!this.state.validContent}
                        helperText={!this.state.validContent && <FormattedMessage id={"fieldError1"}/>}
                        onChange={(e) => this.setState({content: e.target.value})}
                    />
                </DialogContent>
                <DialogActions>
                    <Button  color="inherit" onClick={this.props.handleCancel} className={"tuteeAdButton buttons"}>
                        <FormattedMessage id={"CreateTuteeAd.cancelButton"}/>
                    </Button>
                    <Button  color="inherit" onClick={this.onSubmit} className={"tuteeAdButton buttons"}>
                        <FormattedMessage id={"CreateTuteeAd.submitButton"}/>
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}
