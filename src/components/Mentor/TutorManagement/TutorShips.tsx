import React, { Component } from "react";
import axios from "axios";
import {FormattedMessage} from "react-intl";
import {authenticationService} from "../../../_services/authenticationService";
import {
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper,
    Typography
} from "@material-ui/core";
import "./Tutors.css"
import "./ManageTutors.css"



interface State {
    tutorShips: TutorShip[],
}

interface TutorShip{
    advertisementID: number,
    creationTimestamp: string,
    id: number,
    tuteeID: number,
    tuteeLastName: string,
    tuteeFirstName: string,
    tutorID: number,
    tutorLastName: string,
    tutorFirstName: string,
    updateTimestamp: string
}

/**
 * @koen laermans
 * @description Shows a list of all tutorships
 */
export default class TutorShips extends Component<{}, State> {

    state:State;

    constructor (props: {}){
        super(props);
        this.state = {
            tutorShips:[]
        };

    }

    /**
     * @description fetches all tutorships from the database
     */
    async componentDidMount() {

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const TutorShipsAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/tutorships",
            responseType : "json",
        });

        let tutorShips:TutorShip[] = (await TutorShipsAPI.get("/")).data.data;

        this.setState({
            tutorShips
        });

    }


    render() {
        let tutorShipCount:number = this.state.tutorShips.length;

        return (
            <React.Fragment>
                <Paper className={"tutorPaper"} elevation={4}>
                    <List className={"tutorList"}>
                        <ListItem divider className={"listTitle"}>
                            <Grid container>
                                <Grid item >
                                    <FormattedMessage id={"Tutorships.tutorships"}>
                                        {text =>
                                            <ListItemText><Typography
                                                variant="h6">{text}{tutorShipCount}</Typography></ListItemText>
                                        }
                                    </FormattedMessage>
                                </Grid>
                            </Grid>
                        </ListItem>
                        <ListItem divider>
                            <Grid container>
                                <Grid item xs={4} >
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="Tutorships.tutor"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={4}>
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="Tutorships.tutee"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={3} >
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="Tutorships.created"/>
                                    </Typography></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>

                        {this.state.tutorShips.map((tutorShip:TutorShip, index) => (
                            <ListItem key={tutorShip.id} divider className={"pendingTutorListBodyItem"}>
                                <Grid container>
                                    <Grid item xs={4}>
                                        <ListItemText><Typography >{tutorShip.tutorFirstName +" "+ tutorShip.tutorLastName}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={4}>
                                        <ListItemText><Typography >{tutorShip.tuteeFirstName + " "+tutorShip.tuteeLastName}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <ListItemText><Typography >{new Date(tutorShip.creationTimestamp).toLocaleDateString()}</Typography></ListItemText>
                                    </Grid>
                                </Grid>
                            </ListItem>
                        ))}
                    </List>
                </Paper>
            </React.Fragment>
        );
    }
}
