import React, { Component } from "react";
import axios from "axios";
import {FormattedMessage} from "react-intl";
import {authenticationService} from "../../../_services/authenticationService";
import {Button, Grid, List, ListItem, ListItemText, Paper, Typography} from "@material-ui/core";
import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';
import "./ManageTutors.css"
import Email from "./Email"


interface State {
    pendingTutors: PendingTutor[],
    relevantTutorInfoOnUpdate:relevantTutorInfoOnUpdate,
    showEmail: boolean,
}

interface relevantTutorInfoOnUpdate{
    id: number,
    firstName: string,
    lastName: string,
    accepted:boolean
}

export default interface PendingTutor{
    id: number,
    firstname: string,
    lastname: string,
    email: string,
    motivation: string,
}

/**
 * @author Koen Laermans
 * @description Shows a list of to be accepted/rejected tutors
 */
export default class ManageTutors extends Component<{}, State> {

    state:State;

    constructor (props: {}){
        super(props);
        this.state = {
            pendingTutors:[],
            relevantTutorInfoOnUpdate:{id:-1,firstName:"",lastName:"",accepted:false},
            showEmail:false,
        };
        this.updateTutorAcceptance = this.updateTutorAcceptance.bind(this);
        this.handleAction = this.handleAction.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }


    /**
     * @description Fetches all pending tutors
     */
    async componentDidMount() {
        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const UsersAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users",
            responseType : "json",
        });

        let pendingTutors:PendingTutor[] = (await UsersAPI.get("/tutors/pending/")).data.data;
        this.setState({
            pendingTutors
        })
    }

    /**
     * @description Hides the email form
     */
    handleCancel():void{
        this.setState({
            showEmail:false
        })
    }

    /**
     * @description Fetches all pending tutors
     * @email The email of the tutor
     */
    async updateTutorAcceptance(email:string){

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const UsersAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users/tutors",
            responseType : "json",
        });

        await UsersAPI.post("/" +(this.state.relevantTutorInfoOnUpdate.accepted ? "accept/" : "refuse/")+ this.state.relevantTutorInfoOnUpdate.id.toString(), {
            message:email
        });
        let pendingTutors: PendingTutor[] = this.state.pendingTutors;
        for (let i=0;i<pendingTutors.length;i++){
            if (pendingTutors[i].id === this.state.relevantTutorInfoOnUpdate.id){
                pendingTutors.splice(i,1);
                break;
            }
        }
        this.setState({
            pendingTutors,
            showEmail:false
        })
    }

    /**
     * @description shows an email form
     */
    handleAction(id:number,firstName:string, lastName:string, accepted:boolean):void{

        this.setState({
            relevantTutorInfoOnUpdate:{
                id,
                firstName,
                lastName,
                accepted
            },
            showEmail:true
        });
    }

    render() {

        const emailProps:relevantTutorInfoOnUpdate = this.state.relevantTutorInfoOnUpdate;
        const pendingTutorCount:number = this.state.pendingTutors.length;

        return (
            <React.Fragment>
                <Paper className={"pendingTutorPaper"} elevation={4}>
                    <List className={"pendingTutorList"}>
                        <ListItem divider className={"listTitle"}>
                            <Grid container>
                                <Grid item >
                                    <FormattedMessage id={"ManageTutors.currentapplications"}>
                                        {text =>
                                            <ListItemText><Typography
                                                variant="h6">{text}{pendingTutorCount}</Typography></ListItemText>
                                        }
                                            </FormattedMessage>
                                </Grid>
                            </Grid>
                        </ListItem>
                        <ListItem divider >
                            <Grid container>
                                <Grid item xs={2} >
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="ManageTutors.firstname"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={2}>
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="ManageTutors.lastname"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={3} >
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="ManageTutors.email"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={3} >
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="ManageTutors.motivation"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={2} >
                                    <ListItemText ><Typography variant="h6" className={"pendingTutorListHeaderItem actions"}>
                                        <FormattedMessage id="ManageTutors.actions"/>
                                    </Typography></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>

                        {this.state.pendingTutors.map((tutor: PendingTutor, index) => (
                            <ListItem key={tutor.id} divider className={"pendingTutorListBodyItem"}>
                                <Grid container>
                                    <Grid item xs={2}>
                                        <ListItemText><Typography >{tutor.firstname}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <ListItemText><Typography >{tutor.lastname}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <ListItemText><Typography >{tutor.email}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <ListItemText><Typography >{tutor.motivation}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={2}>
                                        <Button className={"actionButton acceptButton"} onClick={()=>this.handleAction(tutor.id, tutor.firstname, tutor.lastname, true)}><CheckIcon/></Button>
                                        <Button className={"actionButton rejectButton"} onClick={()=>this.handleAction(tutor.id, tutor.firstname, tutor.lastname, false)}><ClearIcon/></Button>
                                    </Grid>
                                </Grid>
                            </ListItem>
                        ))}
                    </List>
                </Paper>
                {this.state.showEmail && (
                    <Email
                        accepted={emailProps.accepted}
                        firstName={emailProps.firstName}
                        lastName={emailProps.lastName}
                        updateTutorAcceptance={this.updateTutorAcceptance}
                        handleCancel={this.handleCancel}
                    />)
                }
            </React.Fragment>
        );
    }
}
