import React, { Component } from "react";
import axios from "axios";
import {FormattedMessage} from "react-intl";
import {authenticationService} from "../../../_services/authenticationService";
import {
    FormControl,
    Grid, InputLabel,
    List,
    ListItem,
    ListItemText, MenuItem,
    Paper, Select,
    Typography
} from "@material-ui/core";
import "./Tutors.css"
import "./ManageTutors.css"
import ProgrammeSelection from "../../tutee-ads/ProgrammeSelection";
import PendingTutor from "./ManageTutors";


interface State {
    tutors: Tutor[],
    pendingTutors: PendingTutor[],
    sortValue: number
    sortSelectMap:{[value: number] : {tutorProperty:string,translation:string}},
    selectedProgramme : string,
}

interface Tutor{
    id:number
    firstname: string,
    lastname: string,
    email: string,
    programme:string
}

/**
 * @author Koen Laermans
 * @description Shows a list of all tutors in the system
 */
export default class Tutors extends Component<{}, State> {

    state:State;

    constructor (props: {}){
        super(props);
        this.state = {
            tutors:[],
            pendingTutors:[],
            sortValue:0,
            sortSelectMap:{0:{tutorProperty:"firstname",translation:"ManageTutors.firstname"},1:{tutorProperty:"lastname",translation:"ManageTutors.lastname"}},
            selectedProgramme:authenticationService.getProgramme() as string,
        };
        this.handleSelectProgram = this.handleSelectProgram.bind(this);
        this.onSortChange = this.onSortChange.bind(this);
        this.sortTutors = this.sortTutors.bind(this);
        this.compareFunction = this.compareFunction.bind(this);
    }


    /**
     * @description handles the selecting of a different program to sort the tutors on
     * @param programme the name of the programme
     */
    handleSelectProgram(programme:string){
        if (programme!==this.state.selectedProgramme) {
            this.setState({
                selectedProgramme: programme
            })
        }
    }

    /**
     * @description Fetches all tutors from the database
     */
    async componentDidMount() {

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const UsersAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users",
            responseType : "json",
        });

        let tutors:Tutor[] = (await UsersAPI.get("/tutors/")).data.data;
        let pendingTutors:PendingTutor[] = (await UsersAPI.get("/tutors/pending/")).data.data;

        this.setState({
            tutors,
            pendingTutors
        });
        this.sortTutors();
    }

    /**
     * @description comparefunction to sort tutors by name
     */
    compareFunction(tutorA:Tutor, tutorB:Tutor):number{
        // @ts-ignore
        return tutorA[this.state.sortSelectMap[this.state.sortValue]["tutorProperty"]].localeCompare(tutorB[this.state.sortSelectMap[this.state.sortValue]["tutorProperty"]]);
    }

    /**
     * @description sorts tutors by name
     */
    sortTutors():void{
        let tutors:Tutor[] = this.state.tutors;
        tutors.sort(this.compareFunction);
        this.setState({
            tutors
        });
    }

    /**
     * @description handle change sort of tutor names
     * @param sortValue: the index of the sorttype to sort on
     */
    onSortChange(sortValue:number):void{
        this.setState({
            sortValue
        }, () => {
            this.sortTutors();
        });
    }


    render() {
        let tutorCount = this.state.tutors.length - this.state.pendingTutors.length;
        let pendingTutorIDs:number[]=[];

        const pendingTutors:PendingTutor[] = this.state.pendingTutors;

        for (let i=0;i<pendingTutors.length;i++){
            pendingTutorIDs = pendingTutorIDs.concat(pendingTutors[i].id);
        }

        return (
            <React.Fragment>
                <Paper className={"tutorPaper"} elevation={4}>
                    <List className={"tutorList "}>
                        <ListItem divider className={"listTitle"}>
                            <Grid container>
                                <Grid item >
                                    <FormattedMessage id={"Tutors.activetutors"}>
                                        {text =>
                                            <ListItemText><Typography
                                                variant="h6">{text} {' : '}{tutorCount}</Typography></ListItemText>
                                        }
                                    </FormattedMessage>
                                </Grid>
                                <Grid item xs>
                                    <FormControl className={"sortSelection"}>
                                        <InputLabel htmlFor="age-native-simple">
                                            <FormattedMessage id="Tutors.sortby"/>
                                        </InputLabel>

                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={this.state.sortValue}
                                            onChange={(e) => this.onSortChange(e.target.value as number)}
                                        >

                                            {Object.entries(this.state.sortSelectMap)
                                                .map(([key, {tutorProperty, translation}]) => (
                                                    <MenuItem key={tutorProperty} value={key}>
                                                        <FormattedMessage id={translation}/>
                                                    </MenuItem>))
                                            }
                                        </Select>

                                    </FormControl>
                                </Grid>
                                <Grid item >
                                    <ProgrammeSelection handleSelectProgram={this.handleSelectProgram} selectedProgramme={this.state.selectedProgramme}/>
                                </Grid>
                            </Grid>
                        </ListItem>
                        <ListItem divider >
                            <Grid container>
                                <Grid item xs={2} >
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="ManageTutors.firstname"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={3}>
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="ManageTutors.lastname"/>
                                    </Typography></ListItemText>
                                </Grid>
                                <Grid item xs={6} >
                                    <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>
                                        <FormattedMessage id="ManageTutors.email"/>
                                    </Typography></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>

                        {this.state.tutors.map((tutor:Tutor, index) => (
                            ((pendingTutorIDs.indexOf(tutor.id) === -1 && tutor.programme === this.state.selectedProgramme) &&
                            <ListItem key={tutor.id} divider className={"pendingTutorListBodyItem"}>
                                <Grid container>
                                    <Grid item xs={2}>
                                        <ListItemText><Typography >{tutor.firstname}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <ListItemText><Typography >{tutor.lastname}</Typography></ListItemText>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <ListItemText><Typography >{tutor.email}</Typography></ListItemText>
                                    </Grid>
                                </Grid>
                            </ListItem>)
                        ))}
                    </List>
                </Paper>
            </React.Fragment>
        );
    }
}
