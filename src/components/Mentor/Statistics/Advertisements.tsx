import React from "react";
import {Typography, List, ListItem, Grid, ListItemText, Paper} from "@material-ui/core";
import TuteeAdListItem from "../../tutee-ads/TuteeAdList";
import {FormattedMessage} from "react-intl";


interface Props{
    advertisements: TuteeAdListItem[],
    semester:string,
    endOfFirstSemester:string
}

export default function Advertisements(props:Props) {

    return (
        <Paper className={"stats"} elevation={4}>
            <List className={"statList"}>
                <ListItem divider className={"listTitle"}>
                    <Grid container>
                        <Grid item >
                            <ListItemText>
                                <Typography
                                    variant="h6"><FormattedMessage id={"advertisements.titleA"}/>{props.semester} semester ({(props.semester === "first" || props.semester === "eerste") ? <FormattedMessage id={"advertisements.until"}/> : <FormattedMessage id={"advertisements.starting"}/>} {new Date(props.endOfFirstSemester).toDateString()}): {props.advertisements.length}
                                </Typography>
                            </ListItemText>
                        </Grid>
                    </Grid>
                </ListItem>
                <ListItem divider >
                    <Grid container>
                        <Grid item xs={5} >
                            <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}><FormattedMessage id={"advertisements.subject"}/></Typography></ListItemText>
                        </Grid>
                        <Grid item xs={3} >
                            <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}><FormattedMessage id={"advertisements.createdOn"}/></Typography></ListItemText>
                        </Grid>
                        <Grid item xs={3} >
                            <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}><FormattedMessage id={"advertisements.acceptedOn"}/></Typography></ListItemText>
                        </Grid>
                    </Grid>
                </ListItem>
                {props.advertisements.map((ad: TuteeAdListItem, index) => (
                    <ListItem key={ad.id} divider>
                        <Grid container>
                            <Grid item xs={5}>
                                <ListItemText><Typography >{ad.title}</Typography></ListItemText>
                            </Grid>
                            <Grid item xs={3}>
                                <ListItemText><Typography >{new Date(ad.creationTimestamp).toLocaleDateString()}</Typography></ListItemText>
                            </Grid>
                            <Grid item xs={3}>
                                <ListItemText><Typography >{ad.accepted ? ad.acceptedDate.toLocaleDateString() : "---"}</Typography></ListItemText>
                            </Grid>
                        </Grid>
                    </ListItem>
                ))}
            </List>
        </Paper>
    );
}

