import React, { Component } from "react";
import { Grid, List, ListItem, ListItemText, Paper, Typography} from "@material-ui/core";
import SessionStatistics from "./SessionStats"
import './Statistics.css'
import {FormattedMessage} from "react-intl";
import TutorShips from "../TutorManagement/TutorShips";
import Advertisements from "./Advertisements";
import Tutors from "./Tutors";

interface Props {

}

interface State {
    ss: SessionStatistics,
}

export default class Statistics extends Component<Props, State> {

    state:State;

    constructor (props: Props){
        super(props);
        this.state = {
            ss: new SessionStatistics(),
        };
    }


    async componentDidMount() {
        // ss = new SessionStatistics();
        //const ss: SessionStatistics = (await UsersAPI.get("/tutors/pending/")).data.data;
        const ss: SessionStatistics = new SessionStatistics();
        await ss.initialize();
        this.setState({ss});

    }

    render() {
        return (
            <React.Fragment>

                <Tutors
                    tutors={this.state.ss.tutors}
                    averageSessionDuration={this.state.ss.averageSessionDuration}
                />

                <Paper className={"stats"} elevation={4}>
                    <List className={"statList"}>
                        <ListItem divider className={"listTitle"}>
                            <Grid container>
                                <Grid item >
                                    <ListItemText>
                                        <Typography variant="h6">
                                            Tutees: {this.state.ss.tuteeCount}
                                        </Typography>
                                    </ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>
                    </List>
                </Paper>

                <div className={"tutorshipStatistics"}>
                    <TutorShips/>
                </div>

                <FormattedMessage id={"statistics.semester1"}>
                    {translation=>
                        <Advertisements
                            advertisements={this.state.ss.getAdvertisementsOfFirstSemester()}
                            endOfFirstSemester={this.state.ss.endOfFirstSemester}
                            semester={translation as string}
                        />
                    }
                </FormattedMessage>
                <FormattedMessage id={"statistics.semester2"}>
                    {translation =>
                        <Advertisements
                            advertisements={this.state.ss.getAdvertisementsOfSecondSemester()}
                            endOfFirstSemester={this.state.ss.endOfFirstSemester}
                            semester={translation as string}
                        />
                    }
                </FormattedMessage>
            </React.Fragment>
        );
    }
}
