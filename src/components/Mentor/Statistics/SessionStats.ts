import axios from "axios"
import {Session, Tutorship} from "../../../_helpers/backend-data";
import {authenticationService} from "../../../_services/authenticationService";
import TuteeAdListItem from "../../tutee-ads/TuteeAdList";

interface Tutor {
  "tutorID" : number,
  "tutorName" : string,
  "sessions" : number,
  "email" : string,
  "programme":string
}


export default class SessionStatistics {
  readonly averageSessionDuration:number;
  readonly endOfFirstSemester:string;
  readonly endOfSecondSemester:string;

  private sessions: Session[];
  tutorships: Tutorship[];
  tutors: Tutor[];
  private advertisements:TuteeAdListItem[];
  tuteeCount: number;
  // private AUTH_TOKEN: string | null;

  constructor() {
    this.averageSessionDuration = 1.5;
    this.endOfFirstSemester = this.getEndOfFirstSemester();
    this.endOfSecondSemester = this.getEndOfSecondSemester();
    this.sessions = [];
    this.tutorships = [];
    this.tutors = [];
    this.advertisements = [];

    this.tuteeCount = 0;
  }

  private getEndOfFirstSemester():string{
    let date:Date = new Date();
    date.setMonth(1,1); //februari 1st
    return date.toLocaleDateString();
  }

  private getEndOfSecondSemester():string{
    let date:Date = new Date();
    date.setMonth(6,1); //februari 1st
    return date.toLocaleDateString();
  }

  private setSessions(sessions: Session[]): void {
    sessions.sort((a: Session, b: Session) => (a.date > b.date) ? 1 : -1);
    this.sessions = sessions;
  }

  private setTutorships(tutorships: Tutorship[]): void {
    this.tutorships = tutorships;
  }

  private setTutors(tutors: Tutor[]): void {
    this.tutors = tutors;
  }

  private setAdvertisements(advertisements:TuteeAdListItem[]):void{
    this.advertisements = advertisements;
  }

  async initialize() {
    await this.loadSessions();
    await this.loadTutors();
    await this.loadTutorships();
    await this.getTuteeCount();
    await this.loadAdvertisements();
    this.loadSessionsByTutor();
  }

  private async getTuteeCount(){
    const AUTH_TOKEN: string | null = authenticationService.getToken();
    axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;
    const usersAPI = axios.create({
      baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users",
      responseType : "json",
    });
    const students = (await usersAPI.get("/students/")).data.data;
    for (let i=0;i<students.length;i++){
      if (students[i].role === "tutee")
        this.tuteeCount++;
    }
  }

  getAdvertisementsOfFirstSemester(): TuteeAdListItem[]{
      let result:TuteeAdListItem[] = [];

      for (let i=0;i<this.advertisements.length;i++){
        let creationDate:Date = new Date(this.advertisements[i].creationTimestamp);
        let endOfFirstSemesterDate:Date = new Date(this.endOfFirstSemester);
        let endOfSecondSemesterDate:Date = new Date(this.endOfSecondSemester);

        if ((creationDate.getMonth() <= endOfFirstSemesterDate.getMonth() && creationDate.getDay() < endOfFirstSemesterDate.getDay()) ||
            (creationDate.getMonth() >= endOfSecondSemesterDate.getMonth() && creationDate.getDay() >= endOfSecondSemesterDate.getDay()))
            result.push(this.advertisements[i])
      }

      return result;
  }

  getAdvertisementsOfSecondSemester(): TuteeAdListItem[]{
    let result:TuteeAdListItem[] = [];
    for (let i=0;i<this.advertisements.length;i++){
      let creationDate:Date = new Date(this.advertisements[i].creationTimestamp);
      let endOfFirstSemesterDate:Date = new Date(this.endOfFirstSemester);
      let endOfSecondSemesterDate:Date = new Date(this.endOfSecondSemester);

      if ((creationDate.getMonth() >= endOfFirstSemesterDate.getMonth() && creationDate.getDay() >= endOfFirstSemesterDate.getDay()) ||
          (creationDate.getMonth() <= endOfSecondSemesterDate.getMonth() && creationDate.getDay() < endOfSecondSemesterDate.getDay()))
        result.push(this.advertisements[i])
    }
    return result;
  }

  async loadAdvertisements(){
    axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();
    const AdvertisementsAPI = axios.create({
      baseURL : process.env.REACT_APP_BACKEND_API_URL + "/applications",
      responseType : "json",
    });

    let advertisements:TuteeAdListItem[] = (await AdvertisementsAPI.get('/')).data.data;
    let acceptedTutorshipInfo:{adId: number, creationDate:Date}[] = [];

    for (let i=0;i<this.tutorships.length;i++)
      acceptedTutorshipInfo.push({adId:this.tutorships[i].advertisementID, creationDate:new Date(this.tutorships[i].creationTimestamp)});

    for (let i=0;i<advertisements.length;i++){
        for (let j=0;j<acceptedTutorshipInfo.length;j++){
            if (advertisements[i].id === acceptedTutorshipInfo[j].adId) {
                advertisements[i].accepted = true;
                advertisements[i].acceptedDate = acceptedTutorshipInfo[j].creationDate;
                break;
            }
        }
    }

    this.setAdvertisements(advertisements);
  }

  async loadSessions() {
    const AUTH_TOKEN: string | null = authenticationService.getToken();
    axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;
    const sessionsAPI = axios.create({
      baseURL : process.env.REACT_APP_BACKEND_API_URL + "/sessions",
      responseType : "json",
    });
    const result = (await sessionsAPI.get("/")).data;
    this.setSessions(result.data);
  }

  async loadTutors() {
    const AUTH_TOKEN: string | null = authenticationService.getToken();
    axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;
    const tutorsAPI = axios.create({
      baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users/tutors",
      responseType : "json",
    });
    const result = (await tutorsAPI.get("/")).data;

    const tutors: Tutor[] = [];
    for (const tutorObject of result.data) {
      tutors.push({"tutorID": tutorObject.id,
                   "tutorName": tutorObject.firstname + " " + tutorObject.lastname,
                   "sessions": 0,
                   "email":tutorObject.email,
                  "programme":tutorObject.programme
      });
    }
    this.setTutors(tutors);
  }

  async loadTutorships() {
    const AUTH_TOKEN: string | null = authenticationService.getToken();
    axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;
    const tutorshipsAPI = axios.create({
      baseURL : process.env.REACT_APP_BACKEND_API_URL + "/tutorships",
      responseType : "json",
    });
    const result = (await tutorshipsAPI.get("/")).data;
    this.setTutorships(result.data);
  }

  public getSessions(): Session[] {
    return this.sessions;
  }

  private loadSessionsByTutor() {
    for (const tutorship of this.tutorships) {
      let tutor = this.tutors.filter(tutor => (tutor.tutorID === tutorship.tutorID))[0];
      if (tutor) {
        for (const session of this.sessions) {
          if (session.tutorshipID === tutorship.id) {
            tutor.sessions++;
          }
        }
      }
    }
  }
}
