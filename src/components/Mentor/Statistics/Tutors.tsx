import React, {Component} from "react";
import {Typography, List, ListItem, Grid, ListItemText, Paper} from "@material-ui/core";
import {FormattedMessage} from "react-intl";
import PendingTutor from "../TutorManagement/ManageTutors";
import {authenticationService} from "../../../_services/authenticationService";
import ProgrammeSelection from "../../tutee-ads/ProgrammeSelection";


interface Props{
    tutors: any[],
    averageSessionDuration:number,
}
interface State {
    selectedProgramme : string,
}

export default class Tutors extends Component<Props, State> {
    state:State;

    constructor (props: Props){
        super(props);
        this.state = {
            selectedProgramme:authenticationService.getProgramme() as string,
        };
        this.handleSelectProgram = this.handleSelectProgram.bind(this);
    }


    /**
     * @description handles the selecting of a different program to sort the tutors on
     * @param programme the name of the programme
     */
    handleSelectProgram(programme:string){
        if (programme!==this.state.selectedProgramme) {
            this.setState({
                selectedProgramme: programme
            })
        }
    }

    render(){
        return (
            <Paper className={"stats"} elevation={4}>
                <List className={"statList"}>
                    <ListItem divider className={"listTitle"}>
                        <Grid container>
                            <Grid item xs>
                                <ListItemText>
                                    <Typography
                                        variant="h6"><FormattedMessage id={"tutors.title"}/>: {this.props.tutors.length}
                                    </Typography>
                                </ListItemText>
                            </Grid>
                            <Grid item >
                                <ProgrammeSelection handleSelectProgram={this.handleSelectProgram} selectedProgramme={this.state.selectedProgramme}/>
                            </Grid>
                        </Grid>
                    </ListItem>
                    <ListItem divider >
                        <Grid container>
                            <Grid item xs={2} >
                                <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>Tutor</Typography></ListItemText>
                            </Grid>
                            <Grid item xs={5} >
                                <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}>Email</Typography></ListItemText>
                            </Grid>
                            <Grid item xs={2}>
                                <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}><FormattedMessage id={"tutors.sessions"}/></Typography></ListItemText>
                            </Grid>
                            <Grid item xs={3}>
                                <ListItemText><Typography variant="h6" className={"pendingTutorListHeaderItem"}><FormattedMessage id={"tutors.sessionTime"}/></Typography></ListItemText>
                            </Grid>
                        </Grid>
                    </ListItem>
                    {this.props.tutors.map((tutor: any, index) => (
                        tutor.programme === this.state.selectedProgramme &&
                        <ListItem key={tutor.id} divider >
                            <Grid container>
                                <Grid item xs={2}>
                                    <ListItemText><Typography >{tutor.tutorName}</Typography></ListItemText>
                                </Grid>
                                <Grid item xs={5}>
                                    <ListItemText><Typography >{tutor.email}</Typography></ListItemText>
                                </Grid>
                                <Grid item xs={2}>
                                    <ListItemText><Typography >{tutor.sessions}</Typography></ListItemText>
                                </Grid>
                                <Grid item xs={3}>
                                    <ListItemText><Typography >{tutor.sessions*this.props.averageSessionDuration}<FormattedMessage id={"statistics.hours"}/></Typography></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>
                    ))}
                </List>
            </Paper>
        );
    }
}

