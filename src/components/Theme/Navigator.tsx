import React, {Component} from 'react'
import HomeIcon from '@material-ui/icons/Home';
import CloseIcon from '@material-ui/icons/Close';
import MenuIcon from '@material-ui/icons/Menu';
import {
    ListItemIcon,
    Drawer,
    List,
    ListItem,
    ListItemText,
    Divider,
    Grid,
    Hidden,
    Typography
} from "@material-ui/core"
import "./Navigator.css"
import AllConversations from "./AllConversations";
import Navigations from "./Navigations";
import {Link} from "react-router-dom";
import {authenticationService} from "../../_services/authenticationService";
import IconButton from "@material-ui/core/IconButton";
import {FormattedMessage} from "react-intl";

interface Props{
    onMobile:boolean,
    handleRouteChange:(newRoute:string)=>void,
    pageName:string
}

interface State {
    redirectToHome:boolean,
    openDrawer: boolean,
    drawerWidth: number,
}

/**
 * @description A drawer that contains all pages and conversations that a user can navigate to.
 * @author Koen Laermans
 */
export default class Navigator extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            redirectToHome:false,
            openDrawer: !props.onMobile,
            drawerWidth:250,
        };

        this.onToggleDrawer = this.onToggleDrawer.bind(this);
    }


    onToggleDrawer():void{
        console.log("toggled");
        this.setState({
            openDrawer: !this.state.openDrawer
        })
    }

    componentWillReceiveProps(newProps:Props) {
        const openDrawer:boolean = this.state.openDrawer;
        if (newProps.onMobile === openDrawer){
            this.setState({
                    openDrawer:!newProps.onMobile
                }
            );
        }
    }


    render() {
        return (
            <div className={""+((this.state.openDrawer && !this.props.onMobile) && "openDrawer")}>
                <Hidden smUp>
                    <Grid item className={"menuIcon"}>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={this.onToggleDrawer}
                        >
                            {this.state.openDrawer ? <CloseIcon/>: <MenuIcon />}
                        </IconButton>
                    </Grid>
                </Hidden>

                <Drawer
                    variant={(this.props.onMobile ? "temporary" : "persistent")}
                    open={(this.state.openDrawer && this.props.onMobile) || !this.props.onMobile}
                    onClose={this.onToggleDrawer}
                    PaperProps={{ style: { width: this.state.drawerWidth } }}
                >
                    <List disablePadding>

                        <ListItem className={"firebase item itemCategory"}>
                            <ListItemText className={"platformTitle"}>
                                <Typography variant="h6"  align="center" >
                                    UHasselt Tutoring
                                </Typography>
                            </ListItemText>
                        </ListItem>


                        <Link to={{
                            pathname:"/",
                        }}
                              className={"Link"}
                        >
                            <ListItem className={"item itemCategory"} >
                                <ListItemIcon className={"itemIcon"}>
                                    <HomeIcon />
                                </ListItemIcon>
                                <ListItemText className={"itemPrimary"}>
                                    Home
                                </ListItemText>
                            </ListItem>
                        </Link>


                        {authenticationService.getToken() != null && (
                            <React.Fragment>
                                <Navigations toggleDrawer={this.onToggleDrawer} onMobile={this.props.onMobile} handleRouteChange={this.props.handleRouteChange}/>
                                <Divider className={"divider"} />
                                <AllConversations toggleDrawer={this.onToggleDrawer} onMobile={this.props.onMobile} handleRouteChange={this.props.handleRouteChange} pageName={this.props.pageName}/>
                            </React.Fragment>
                        )}
                    </List>
                </Drawer>
            </div>
        );
    }
}


