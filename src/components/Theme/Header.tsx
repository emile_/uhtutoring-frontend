import React, {Component} from 'react'
import {Toolbar, AppBar, Grid, Typography} from "@material-ui/core"
import "./Header.css"
import LanguageDropdown from '../language-dropdown/LanguageDropdown';
import NotificationsDropdown from '../notifications-dropdown/NotificationsDropdown';
import AccountDropdown from '../account-dropdown/AccountDropdown'
import {authenticationService} from "../../_services/authenticationService";
import Hidden from "@material-ui/core/Hidden";

interface Props {
    language: string
    onChangeLanguage : (language : string) => void
    languages: string[]
    languagesIcons : Map<string,string>,
    pageName:string
}

interface State {
    loggedIn : boolean
}

/**
 * @description Shows the header, which contains a language dropdown, an alert icon and a profile icon
 */
export default class Header extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);
        this.state = {
            loggedIn : authenticationService.getToken()!=null,
        };

        this.renderGridItemOnLogin = this.renderGridItemOnLogin.bind(this);
    }

    renderGridItemOnLogin(element: JSX.Element) {
        if (this.state.loggedIn) {
            return <Grid item> {element} </Grid>
        }
    }

    render() {

        return (
            <React.Fragment>
                <AppBar color="primary" position="fixed" elevation={2}>
                    <Toolbar>
                        <Grid container spacing={1} alignItems="center">

                            <Hidden smUp implementation="js">
                                <div className={"closedDrawer"}/>
                            </Hidden>
                            <Hidden xsDown implementation="css">
                                <div className={"openDrawer"}/>
                            </Hidden>

                            <Grid item xs ><Typography variant={"h5"}>{this.props.pageName}</Typography></Grid>
                            <Grid item>
                                <LanguageDropdown
                                    language={this.props.language}
                                    onChangeLanguage={this.props.onChangeLanguage}
                                    languages={this.props.languages}
                                    languagesIcons={this.props.languagesIcons}
                                />
                            </Grid>
                                {this.renderGridItemOnLogin(<NotificationsDropdown/>)}
                                {this.renderGridItemOnLogin(<AccountDropdown/>)}
                            </Grid>
                    </Toolbar>
                </AppBar>
            </React.Fragment>

        );
    }
}
