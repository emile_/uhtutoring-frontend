import React from "react";
import {Typography, Link} from "@material-ui/core";
import "./Footer.css";

/**
 * @description Shows the footer
 */
export default function Footer() {

    return (
        <Typography variant="body2" color="textSecondary" align="center" className={"footer"}>
            {'Copyright © '}
            <Link color="inherit" href="https://www.uhasselt.be">
                UHasselt
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

