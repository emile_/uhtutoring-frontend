import React, {Component} from "react";
import {ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import "./Footer.css";
import PeopleIcon from "@material-ui/icons/People";
import EventIcon from "@material-ui/icons/Event";
import AnnouncementIcon from '@material-ui/icons/Announcement';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import  RateReviewIcon from '@material-ui/icons/RateReview';
import AddBox from '@material-ui/icons/AddBox'
import {FormattedMessage} from "react-intl";
import {Link} from "react-router-dom";
import {Role, roleToString} from "../../_helpers/role";
import {authenticationService} from "../../_services/authenticationService";


interface Props{
    toggleDrawer: () => void,
    onMobile:boolean,
    handleRouteChange:(newRoute:string)=>void
}

interface State {
    redirect:boolean,
    redirectCategoryIndex: number,
    navigationsByRole: {[title: string] : Navigation[]}
    role:string
}
interface Navigation{
    title:string,
    route:string,
    icon:any,
    active:boolean,
    props:{}
}

/**
 * @description Shows the pages that the user can navigate to.
 * @author Koen Laermans
 */
export default class Navigations extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            redirect:false,
            redirectCategoryIndex:0,
            navigationsByRole: this.getNavigationsByRole(),
            role: roleToString(authenticationService.getRole() as Role)
        };

        this.onClick = this.onClick.bind(this);
    }


    getNavigationsByRole():{[id: string] : Navigation[]}{
        let navigationsByRole:{[id: string] : Navigation[]} = {};
        navigationsByRole["tutee"] = this.getTuteeNavigations();
        navigationsByRole["tutor"] = this.getTutorNavigations();
        navigationsByRole["mentor"] = this.getMentorNavigations();
        navigationsByRole["admin"] = this.getAdminNavigations();
        return navigationsByRole;
    }

    /**
     * @description Returns all navigations visible by a tutor
     */
    getTutorNavigations():Navigation[]{
        return [
            {title: 'Navigations.advertisements', route : "Advertisements", icon: <PeopleIcon />,active:false, props:{}},
            {title: 'Navigations.agenda', route : "Agenda", icon: <EventIcon />,active:false, props:{}},
            {title: 'Navigations.googledrive', route : "GoogleDrive", icon: <PeopleIcon/>, active:false, props:{}}
        ]
    }

    /**
     * @description Returns all navigations visible by a tutee
     */
    getTuteeNavigations():Navigation[]{
        return [
            {title: 'Navigations.CreateAdvertisement', route : "CreateAdvertisement", icon: <AddBox />,active:false,
                props:{
                    title: "",
                    description: "",
                    id: -1
                }
            },
            {title: 'Navigations.advertisements', route: "Advertisements", icon: <PeopleIcon />,active:false, props:{}},
            {title: 'Navigations.agenda', route : "Agenda", icon: <EventIcon />,active:false, props:{}}
        ]
    }

    /**
     * @description Returns all navigations visible by a mentor
     */
    getMentorNavigations():Navigation[]{
        return [
            {title: 'Navigations.managetutors', route : "ManageTutors", icon: <VerifiedUserIcon />,active:false, props:{}},
            {title: 'Navigations.activetutors', route: "ActiveTutors", icon: <PeopleIcon />,active:false, props:{}},
            {title: 'Navigations.tutorships', route: "Tutorships", icon: <SupervisedUserCircleIcon />,active:false, props:{}},
            {title: 'Navigations.reviews', route: "Reviews", icon: <RateReviewIcon/>,active:false, props:{}},
            {title: 'Navigations.announcements', route: "AnnouncementPanel", icon: <AnnouncementIcon/>,active:false, props:{}},
            {title: 'Navigations.advertisements', route: "Advertisements", icon: <PeopleIcon />,active:false, props:{}},
            {title: 'Navigations.statistics', route: "Statistics", icon: <EqualizerIcon />,active:false, props:{}},
        ]
    }

    /**
     * @description Returns all navigations visible by an admin
     */
    getAdminNavigations():Navigation[]{
        return [

        ]
    }


    /**
     * @description Will highlight the active page. Currently not working.
     */
    onClick(redirectCategoryIndex: number){

        let role:string = this.state.role;
        let navigationsByRole: {[title: string] : Navigation[]} = this.state.navigationsByRole;

        if (navigationsByRole[role][redirectCategoryIndex].route !== "GoogleDrive"){
            for (let i=0;i<navigationsByRole[role].length;i++){
                navigationsByRole[role][i].active = (i === redirectCategoryIndex);
            }
            // @ts-ignore
            this.props.handleRouteChange(<FormattedMessage id={navigationsByRole[role][redirectCategoryIndex].title}/>);
        }
        // @ts-ignore
        this.props.handleRouteChange(<FormattedMessage id={navigationsByRole[role][redirectCategoryIndex].title}/>);

        if (this.props.onMobile)
            this.props.toggleDrawer();

        this.setState({
            redirect:true,
            redirectCategoryIndex: redirectCategoryIndex,
            navigationsByRole: navigationsByRole
        })
    }


    render() {

        return (
            <React.Fragment>
                {this.state.navigationsByRole[this.state.role].map(({ title,route, icon, active, props}, index) => (
                    <FormattedMessage id={title} key={route}>

                        {translationtitle =>
                        <Link to={{
                            pathname:"/"+route,
                            state: props
                        }}
                          className={"Link"}
                          key={title}>

                            <ListItem
                                key={title}
                                button
                                className={"item"}
                                onClick={() => this.onClick(index)}
                            >
                                <ListItemIcon className={(active ? "active":" itemIcon")}>{icon}</ListItemIcon>
                                <ListItemText className={(active ? "active":"itemPrimary")}>{translationtitle}</ListItemText>
                            </ListItem>

                        </Link>
                    }
                </FormattedMessage>

                ))}
            </React.Fragment>
        );
    };
}

