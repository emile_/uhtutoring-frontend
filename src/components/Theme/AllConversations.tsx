import React, {Component} from 'react'
import PersonIcon from '@material-ui/icons/Person';
import {ListItemIcon, ListItem, ListItemText, Divider, Badge} from "@material-ui/core"
import axios from 'axios'
import "./Navigator.css"
import {Link} from "react-router-dom";
import {authenticationService} from "../../_services/authenticationService";
import {FormattedMessage} from "react-intl";
import {createMuiTheme} from "@material-ui/core/styles";
import {NamedMuiElement} from "@material-ui/core/utils/isMuiElement";

interface Props{
    toggleDrawer: () => void,
    onMobile:boolean,
    handleRouteChange:(newRoute:string)=>void,
    pageName:string
}

interface State {
    conversations: Conversation[]
    unreadMessages:{[conversationID: string] : number},
    activeConversationID:number
}
interface Conversation{
    id: number,
    name: string
}
/**
 * @description Shows all conversations that a person has with other people. Clicking on a conversation will redirect to the conversation itself.
 * @author Koen Laermans
 */
export default class AllConversations extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            conversations:[],
            unreadMessages:{},
            activeConversationID:-1
        };
        this.handleMessageReceived = this.handleMessageReceived.bind(this);
        this.initWebsocket = this.initWebsocket.bind(this);
        this.onClick = this.onClick.bind(this);
    }


    componentWillReceiveProps(newProps:Props) {
        if (newProps.pageName !== "Chat"){
            this.setState({activeConversationID:-1})
        }
    }


    handleMessageReceived(conversationID:number):void{
        if (this.state.activeConversationID !== conversationID){
            let unreadMessages:{[conversationID: string] : number} = this.state.unreadMessages;
            unreadMessages[conversationID]++;
            this.setState({
                unreadMessages
            })
        }
    }


    initWebsocket(): void {
        const conversations:Conversation[] = this.state.conversations;
        const AUTH_TOKEN:string=authenticationService.getToken() as string;
        for (let i=0;i<conversations.length;i++){
            const ws = new WebSocket(process.env.REACT_APP_BACKEND_WEBSOCKET_URL!);
            ws.onopen = () => {
                console.log('connected');
                ws.send('{ "type": "subscribe", "conversationID": ' + conversations[i].id.toString() + ', "token": "'+ AUTH_TOKEN + '" }');
                setInterval(() => {
                    ws.send('{ "type": "ping", "token": "'+ AUTH_TOKEN + '" }');
                }, 10000);

            };
            ws.onmessage = evt => {
                // listen to data sent from the websocket server
                const message = JSON.parse(evt.data);
                if (message.id !== undefined){
                    this.handleMessageReceived(conversations[i].id);
                }
            };
            ws.onclose = () => {
                console.log('disconnected');
            };
        }
    }

    /**
     * @description Fetches all conversations from the database.
     */
    async componentDidMount(){
        axios.defaults.headers.common["Authorization"] = "Bearer "+authenticationService.getToken();

        const MessageAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/messages",
            responseType : "json",
        });

        let conversations:Conversation[] = (await MessageAPI.get('/conversations')).data.conversations;
        let unreadMessages:{[conversationID: string] : number} = {};
        for (let i=0;i<conversations.length;i++){
            unreadMessages[conversations[i].id] = 0;
        }
        this.setState({
            conversations,
            unreadMessages
        });
        this.initWebsocket();
    }


    onClick(conversationID:number){
        let unreadMessages:{[conversationID: string] : number} = this.state.unreadMessages;
        unreadMessages[conversationID] = 0;
        this.setState({
            activeConversationID:conversationID,
            unreadMessages
        });
        this.props.handleRouteChange("Chat");
        if (this.props.onMobile)
            this.props.toggleDrawer();
    }

    render() {
        let conversations: Conversation[] = this.state.conversations;

        return (

            <React.Fragment>
                <ListItem className={"categoryHeader"}>
                    <ListItemText className={"categoryHeaderPrimary"}>
                        <FormattedMessage id="AllConversations.Conversations"/>
                    </ListItemText>
                </ListItem>
                {conversations.map(({ id, name }) => (
                    <Link to={{
                        pathname:"/chat",
                        state:{
                            id:id,
                            name:name
                        }
                    }}
                    className={"Link"}
                    key={id}>
                        <ListItem
                            key={id}
                            button
                            className={"item "}
                            onClick={() => this.onClick(id)}
                        >
                            <ListItemIcon className={"itemIcon"}><PersonIcon/></ListItemIcon>
                            <Badge badgeContent={this.state.unreadMessages[id]} color="secondary" className={"unreadMessagesBadge"}>
                                <ListItemText className={"itemPrimary"}>{name}</ListItemText>
                            </Badge>
                        </ListItem>
                    </Link>

                ))}
                <Divider className={"divider"} />
            </React.Fragment>
        );
    }
}


