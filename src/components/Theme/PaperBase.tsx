import React, {Component} from 'react';
import {
    createMuiTheme,
    ThemeProvider,
} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Hidden from '@material-ui/core/Hidden';
import './PaperBase.css'
import {NamedMuiElement} from "@material-ui/core/utils/isMuiElement";
import Header from "./Header";
import Navigator from "./Navigator";
import Router from "../Router/Router";
import "./Header.css"
import {authenticationService} from "../../_services/authenticationService";


interface Props {
    language: string
    onChangeLanguage : (language : string) => void
    languages: string[]
    languagesIcons : Map<string,string>
}

interface State {
    loggedIn: boolean,
    themeStyle: NamedMuiElement,
    pageName:string
}

/**
 * @description A Theme that contains a header, footer, drawer and body with it's own style
 */
export default class PaperBase extends Component<Props, State> {

    state: State;

    constructor(props: Props) {
        super(props);

        this.state = {
            loggedIn : false,
            themeStyle : this.initTheme(),
            pageName:this.getPageName(props.language)
        };
        this.handleRouteChange = this.handleRouteChange.bind(this);
        this.getPageName = this.getPageName.bind(this);
    }

    getPageName(language:string):string{
        let pageNames:string[] = ["","Home","Start","Login/Register", "Login/Registreer"];
        if (this.state === undefined || pageNames.includes(this.state.pageName)){
            return (authenticationService.getToken() === null ? (language === "en" ? "Login/Register":"Login/Registreer") : (
                language === "en" ? "Home":"Start"))
        }
        return this.state.pageName;
    }

    initTheme(): any {
        // Theme configuration
        let paperBaseTheme: any = createMuiTheme({
            mixins: {
                toolbar: {
                    minHeight: 48,
                },
            },
            palette: {
                primary: {
                    light: '#63ccff',
                    main: '#009be5',
                    dark: '#006db3',
                },
            },
            typography: {
                h5: {
                    fontWeight: 500,
                    fontSize: 26,
                    letterSpacing: 0.5,
                },
            },
            shape: {
                borderRadius: 8,
            },
            props: {
                MuiTab: {
                    disableRipple: true,
                },
            },
        });

        // More theme configuration
        paperBaseTheme = {
            ...paperBaseTheme,
            overrides: {
                MuiDrawer: {
                    paper: {
                        backgroundColor: '#18202c',
                    },
                },
                MuiButton: {
                    contained: {
                        '&:active': {
                            boxShadow: 'none',
                        },
                        boxShadow: 'none',
                    },
                    label: {
                        textTransform: 'none',
                    },
                },
                MuiTabs: {
                    indicator: {
                        backgroundColor: paperBaseTheme.palette.common.white,
                        borderTopLeftRadius: 3,
                        borderTopRightRadius: 3,
                        height: 3,
                    },
                    root: {
                        marginLeft: paperBaseTheme.spacing(1),
                    },
                    label: {
                        textTransform: 'none',
                    },
                    contained: {
                        boxShadow: 'none',
                        '&:active': {
                            boxShadow: 'none',
                        },
                    },
                },
                MuiTab: {
                    root: {
                        textTransform: 'none',
                        margin: '0 16px',
                        minWidth: 0,
                        padding: 0,
                        [paperBaseTheme.breakpoints.up('md')]: {
                            minWidth: 0,
                            padding: 0,
                        },
                    },
                },
                MuiIconButton: {
                    root: {
                        padding: paperBaseTheme.spacing(1),
                    },
                },
                MuiTooltip: {
                    tooltip: {
                        borderRadius: 4,
                    },
                },
                MuiDivider: {
                    root: {
                        backgroundColor: '#404854',
                    },
                },
                MuiListItemText: {
                    primary: {
                        fontWeight: paperBaseTheme.typography.fontWeightMedium,
                    },
                },
                MuiListItemIcon: {
                    root: {
                        '& svg': {
                            fontSize: 20,
                        },
                        color: 'inherit',
                        marginRight: 0,
                    },
                },
                MuiAvatar: {
                    root: {
                        width: 32,
                        height: 32,
                    },
                },
            },
        };
        return paperBaseTheme;
    }


    handleRouteChange(newRoute:string){
        this.setState({
            pageName:newRoute
        })
    }


    componentWillReceiveProps(newProps:Props) {
        if (newProps.language !== this.props.language){
            this.setState({
                pageName:this.getPageName(newProps.language)
            })
        }
    }


    render() {

        return (
            <ThemeProvider theme={this.state.themeStyle}>
                <div className={"root"}>
                    <CssBaseline />

                    <nav className={"drawer"}>
                        <Hidden smUp implementation="js">
                            <Navigator onMobile={true} handleRouteChange={this.handleRouteChange} pageName={this.state.pageName}/>
                        </Hidden>

                        <Hidden xsDown implementation="css">
                            <Navigator onMobile={false} handleRouteChange={this.handleRouteChange} pageName={this.state.pageName}/>
                        </Hidden>
                    </nav>

                    <div className={"app"} >
                       <div className={"header"}>
                           <Header
                                language={this.props.language}
                                onChangeLanguage={this.props.onChangeLanguage}
                                languages={this.props.languages}
                                languagesIcons={this.props.languagesIcons}
                                pageName={this.state.pageName}
                            />
                       </div>
                        <main className={"main"}>
                            <Router/>
                        </main>

                    </div>

                </div>
            </ThemeProvider>
        );
    }
}
