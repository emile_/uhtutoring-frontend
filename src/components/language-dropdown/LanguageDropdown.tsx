import React, {CSSProperties} from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {FormattedMessage} from 'react-intl';

/**
 * Props
 */
interface IProps {
    language : string
    languagesIcons : Map<string,string>
    onChangeLanguage : (language : string) => void
    languages : string[]
}

/**
 * Provides a dropdown where the user can select his/her preferred language.
 */
export default function LanguageDropdown(props : IProps) {

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    /**
     * Open the dropdown menu
     */
    const handleClick = (event : React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    /**
     * Close the dropdown
     */
    const handleClose = () => {
        setAnchorEl(null);
    };

    /**
     * Handle the choice
     * @param language chosen language
     */
    const handleChoice = (language : string) => {
        handleClose();
        props.onChangeLanguage(language)
    };

    /**
     * All styling properties
     */
    const styleIcon : CSSProperties = {
        marginRight : "6px",
        width : '25px',
    };

    const styleButton : CSSProperties = {
        textTransform : "none",
        backgroundColor : "white"
    };

    /**
     * Render
     */
    return (
        <div>
            <Button aria-controls="simple-menu" variant="contained" aria-haspopup="true" onClick={handleClick} style={styleButton}>
                <img src={props.languagesIcons.get(props.language)} alt={props.language} style={styleIcon}/>
                <FormattedMessage id={"Languages." + props.language}/>
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted={true}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {props.languages.map((language: string) => {
                    return (
                        <MenuItem
                            key={language}
                            onClick={() => handleChoice(language)}
                            href={"#lang/" + language}
                        >
                            <img src={props.languagesIcons.get(language)} alt={language} style={styleIcon}/>
                            <FormattedMessage id={"Languages." + language} defaultMessage={language}/>
                        </MenuItem>
                    );
                })}
            </Menu>
        </div>
    );
}
