import React from "react";
import {SnackbarContent, Snackbar} from "@material-ui/core";

interface Props{
    message:string,
    handleClose:()=>void
}

/**
 * @description Shows an error snackbar at the bottom of the screen with a specified message
 */
export default function ErrorSnackbar(props:Props) {

    return (
        <Snackbar
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
            }}
            open={true}
            autoHideDuration={6000}
            onClose={props.handleClose}
        >
        <SnackbarContent
            className={"errorSnackBar"}
            aria-describedby="client-snackbar"
            message={<span id="client-snackbar">{props.message}</span>}
        />
        </Snackbar>
    );
}

