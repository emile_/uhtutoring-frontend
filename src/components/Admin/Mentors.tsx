import React, { Component } from "react";
import axios from "axios";
import {FormattedMessage} from "react-intl";
import {authenticationService} from "../../_services/authenticationService";
import {
    Button,
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper,
    Typography
} from "@material-ui/core";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import NewMentor from "./NewMentor";
import {Programme} from "./AdminPanel";


interface Props {
    programmes:Programme[],
}

interface State {
    mentors:Mentor[],
    showNewMentor:boolean
}

interface Mentor{
    id:number,
    firstname:string,
    lastname:string,
    email:string,
    role:string,
    programme:string
}


/**
 * @description A list of all mentors
 * @author Koen laermans
 */
export default class Mentors extends Component<Props, State> {

    state:State;

    constructor (props: Props){
        super(props);
        this.state = {
            mentors:[],
            showNewMentor:false
        };
        this.handleCancel = this.handleCancel.bind(this);
        this.updateMentorList = this.updateMentorList.bind(this);
    }


    /**
     * @description Hides the dialog to add new mentor
     */
    handleCancel():void{
        this.setState({
            showNewMentor:false
        })
    }

    /**
     * @description Fetches all mentors from the database
     */
    async updateMentorList(){
        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const UsersAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users",
            responseType : "json",
        });

        let mentors:Mentor[] = (await UsersAPI.get("/mentors/")).data.data;

        this.setState({
            mentors
        });
    }

    /**
     * @description Fetches all mentors from the database
     */
    async componentDidMount() {
        this.updateMentorList();
    }


    render() {
        return (
            <React.Fragment>
                <Paper className={"mentorPaper"} elevation={4}>
                    <List className={"mentorList"}>
                        <ListItem divider className={"mentorListHeader"}>
                            <Typography className={"mentorListHeaderTitle"} ><FormattedMessage id={"mentors.title"}/></Typography>
                            <Grid item className={"addMentorButton"}><Button onClick={()=>this.setState({showNewMentor:true})}><AddCircleOutlineIcon/>
                                <FormattedMessage id="Mentors.add"/>
                            </Button></Grid>
                        </ListItem>
                        {this.state.mentors.map((mentor:Mentor, index) => (
                            <ListItem key={mentor.id} divider className={"mentorListItem"}>
                                <Grid container>
                                    <Grid item md={7}>
                                        <ListItemText>
                                            <Typography >{this.state.mentors.indexOf(mentor)+1+"."+mentor.firstname+" "+mentor.lastname}</Typography>
                                        </ListItemText>
                                    </Grid>
                                    <Grid item className={"mentorProgramme"} xs={4}>
                                        <ListItemText><Typography >{"--"+mentor.programme}</Typography></ListItemText>
                                    </Grid>
                                </Grid>
                            </ListItem>
                        ))}
                    </List>
                </Paper>
                {this.state.showNewMentor && (
                    <NewMentor
                        handleCancel={this.handleCancel}
                        updateMentorList={this.updateMentorList}
                        programmes={this.props.programmes}
                    />)
                }
            </React.Fragment>
        );
    }
}
