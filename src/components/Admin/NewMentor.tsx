import React, {ChangeEvent, Component} from 'react'
import {
    Button,
    TextField,
    FormControl,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Grid,
    InputLabel, Select, MenuItem
} from "@material-ui/core";

import {FormattedMessage} from "react-intl";
import {authenticationService} from "../../_services/authenticationService";
import axios from "axios";
import {Programme} from "./AdminPanel";
import ErrorSnackbar from "../ErrorHandlers/ErrorSnackbar";


interface Props {
    handleCancel:() => void,
    updateMentorList:() => void,
    programmes:Programme[],
}

interface State {
    mentorData:MentorData,
    validInput:ValidInput,
    programmeSelectMap:{[value: number] : string},
    showErrorSnackbar : boolean
}

interface MentorData{
    firstName:string,
    lastName:string,
    email:string,
    password:string,
    repeatPassword:string,
    programmeValue: number,
}

interface ValidInput{
    firstName:boolean,
    lastName:boolean,
    email:boolean,
    password:boolean,
    programme:boolean
}

/**
 * @description A form to add a new mentor
 * @author Koen Laermans
 */
export default class NewMentor extends Component<Props, State> {

    state: State;

    constructor(props: Props){
        super(props);

        this.state = {
            mentorData: {
                firstName: "",
                lastName: "",
                email: "",
                password: "",
                repeatPassword: "",
                programmeValue: 0,
            },
            validInput:{
                firstName:true,
                lastName:true,
                email:true,
                password:true,
                programme:true
            },
            programmeSelectMap:this.initProgrammeSelectMap(this.props.programmes),
            showErrorSnackbar:false
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.isValidInput = this.isValidInput.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    /**
     * @description Adds all programmes to a select dropdown
     * @param programmes: the programmes from the database
     */
    initProgrammeSelectMap(programmes:Programme[]):{[value: number] : string}{
        let programmeSelectMap: {[value: number] : string} = {};
        for (let i=0;i<programmes.length;i++){
            programmeSelectMap[i]=programmes[i].name;
        }
        return programmeSelectMap
    }

    /**
     * @description Handles the closing of an error popup
     */
    handleClose(){
        this.setState({
            showErrorSnackbar:false
        })
    }

    /**
     * @description Handles the change of an input field
     * @param e: the element on which the change has taken place
     */
    onChange(e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>){
        let name:string = e.target.name;
        let value:string = e.target.value;
        let mentorData:MentorData = this.state.mentorData;
        // @ts-ignore
        mentorData[name] = value;
        this.setState({
            mentorData
        })
    }


    /**
     * @description Checks if the content is valid
     */
    isValidInput():boolean{
        let mentorData:MentorData = this.state.mentorData;
        let validInput:ValidInput = this.state.validInput;

        validInput.firstName = mentorData.firstName.length !== 0;
        validInput.lastName = mentorData.lastName.length !== 0;
        validInput.password=!(mentorData.password.length < 6 || mentorData.repeatPassword.length === 0 || mentorData.password!==mentorData.repeatPassword);
        validInput.email = (mentorData.email.length !== 0 && (mentorData.email.split('@')[1]==="uhasselt.be"));

        this.setState({
            validInput
        });

        return (validInput.firstName && validInput.lastName && validInput.email && validInput.password)
    }


    /**
     * @description Handles the submission of a new mentor
     */
    async onSubmit(){
        if (!this.isValidInput()){
            return;
        }

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const UsersAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users",
            responseType : "json",
        });

        let mentorData:MentorData = this.state.mentorData;
        try{
            await UsersAPI.post("/register/mentor",{
                programme : this.state.programmeSelectMap[mentorData.programmeValue],
                role : "mentor",
                password : mentorData.password,
                email : mentorData.email,
                lastname : mentorData.lastName,
                firstname : mentorData.firstName,
            });
        }catch (e) {
            this.setState({
                showErrorSnackbar:true
            });
            return;
        }

        this.props.updateMentorList();
        this.props.handleCancel();
    }


    render() {
        const validInput:ValidInput = this.state.validInput;

        return (
            <Dialog open={true} aria-labelledby="form-dialog-title " PaperProps={{style: { borderRadius: 2,width:"100%"}}} >
                <DialogTitle id="form-dialog-title">
                    <FormattedMessage id={"newMentor.title"}/>
                </DialogTitle>
                <DialogContent>
                    <Grid container>
                    <Grid item xs={6}>
                        <FormattedMessage id={"newMentor.firstname"}>
                            {translation =>
                                <TextField
                                    name="firstName"
                                    id="0"
                                    className={"textField"}
                                    label={translation}
                                    margin="normal"
                                    variant="outlined"
                                    rows={1}
                                    value={this.state.mentorData.firstName}
                                    error={!validInput.firstName}
                                    helperText={!validInput.firstName && <FormattedMessage id="fieldError1"/>}
                                    onChange={(e) => this.onChange(e)}
                                />
                            }
                        </FormattedMessage>
                    </Grid>
                    <Grid item xs={6}>
                        <FormattedMessage id={"newMentor.lastname"}>
                            {translation =>
                            <TextField
                                name="lastName"
                                id="1"
                                className={"textField"}
                                label={translation}
                                margin="normal"
                                variant="outlined"
                                rows={1}
                                value={this.state.mentorData.lastName}
                                error={!validInput.lastName}
                                helperText={!validInput.lastName && <FormattedMessage id="fieldError1"/>}
                                onChange={(e) => this.onChange(e)}
                        />
                            }
                        </FormattedMessage>
                    </Grid>
                        <Grid item xs={6}>
                            <FormattedMessage id={"newMentor.password"}>
                            {translation =>
                                <TextField
                                    name="password"
                                    id="2"
                                    className={"textField"}
                                    label={translation}
                                    margin="normal"
                                    variant="outlined"
                                    type={"password"}
                                    rows={1}
                                    value={this.state.mentorData.password}
                                    error={!validInput.password}
                                    helperText={!validInput.password && <FormattedMessage id="fieldError1"/>}
                                    onChange={(e) => this.onChange(e)}
                                />
                            }
                            </FormattedMessage>
                        </Grid>
                        <Grid item xs={6}>
                            <FormattedMessage id={"newMentor.repeatPassword"}>
                            {translation =>
                                <TextField
                                    name="repeatPassword"
                                    id="3"
                                    className={"textField"}
                                    label={translation}
                                    margin="normal"
                                    variant="outlined"
                                    rows={1}
                                    value={this.state.mentorData.repeatPassword}
                                    type={"password"}
                                    error={!validInput.password}
                                    helperText={!validInput.password && <FormattedMessage id="fieldError1"/>}
                                    onChange={(e) => this.onChange(e)}
                                />
                            }
                            </FormattedMessage>
                        </Grid>
                        <Grid item xs={12}>
                            <FormattedMessage id={"newMentor.email"}>
                            {translation =>
                                <TextField
                                    name="email"
                                    id="4"
                                    className={"textField"}
                                    label={translation}
                                    margin="normal"
                                    variant="outlined"
                                    rows={1}
                                    value={this.state.mentorData.email}
                                    error={!validInput.email}
                                    helperText={!validInput.email && <FormattedMessage id="fieldError1"/>}
                                    onChange={(e) => this.onChange(e)}
                                />
                            }
                            </FormattedMessage>
                        </Grid>
                        <Grid item xs>
                            <FormControl className={"educationSelection"}>
                                <InputLabel htmlFor="age-native-simple"><FormattedMessage id={"newMentor.programme"}/></InputLabel>
                                <Select
                                    name={"programmeValue"}
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.mentorData.programmeValue}
                                    onChange={(e)=>this.onChange(e as ChangeEvent<HTMLTextAreaElement|HTMLInputElement>)}

                                >
                                    {Object.entries(this.state.programmeSelectMap)
                                        .map( ([key, value]) =>(
                                            <MenuItem key={key} value={key}>
                                                {value}
                                            </MenuItem> ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>

                </DialogContent>
                <DialogActions>
                    <Button  color="inherit" onClick={this.props.handleCancel} className={"tuteeAdButton buttons"}><FormattedMessage id="CreateTuteeAd.cancelButton"/></Button>
                    <Button className={"tuteeAdButton buttons"} onClick={this.onSubmit}><FormattedMessage id="CreateTuteeAd.submitButton"/></Button>
                </DialogActions>
                {(this.state.showErrorSnackbar && <ErrorSnackbar message={"Something went wrong!"} handleClose={this.handleClose}/>)}
            </Dialog>

        );
    }
}
