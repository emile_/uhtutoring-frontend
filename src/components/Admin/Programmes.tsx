import React, { Component } from "react";
import {FormattedMessage} from "react-intl";
import {
    Button,
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper, TextField,
    Typography
} from "@material-ui/core";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import {Programme} from "./AdminPanel";


interface Props {
    programmes:Programme[],
    handleAddNewProgramme:(newProgramme:string) => void
}

interface State {
    showAddProgrammeField:boolean,
    newProgrammeValue:string,
    validNewProgramme:boolean
}


export default class Programmes extends Component<Props, State> {

    state:State;

    constructor (props: Props){
        super(props);
        this.state = {
            showAddProgrammeField:false,
            newProgrammeValue:"",
            validNewProgramme:true
        };
        this.toggleAddProgramme = this.toggleAddProgramme.bind(this);
        this.onAddNewProgramme = this.onAddNewProgramme.bind(this);
    }


    toggleAddProgramme():void{
        this.setState({
            showAddProgrammeField:!this.state.showAddProgrammeField
        })
    }

    async onAddNewProgramme(){
        let newProgramme:string = this.state.newProgrammeValue;

        if (newProgramme.length > 1 && newProgramme.length<60){
            this.props.handleAddNewProgramme(newProgramme);
            this.setState({
                validNewProgramme:true,
                newProgrammeValue:"",
                showAddProgrammeField:false
            })
        }else{
            this.setState({
                validNewProgramme:false
            })
        }
    }



    render() {

        return (
            <React.Fragment>
                <Paper className={"programmePaper"} elevation={4}>
                    <List className={"programmeList"}>
                        <ListItem divider className={"mentorListHeader"}>
                            <Typography className={"mentorListHeaderTitle"} >
                                <FormattedMessage id="Programmes.programmes"/>
                            </Typography>
                            {!this.state.showAddProgrammeField &&(
                                <Grid item className={"addMentorButton"}>
                                    <Button onClick={this.toggleAddProgramme}>
                                        <AddCircleOutlineIcon/>
                                        <FormattedMessage id="Programmes.add"/>
                                    </Button>
                                </Grid>)}
                        </ListItem>
                        {this.props.programmes.map((programme:Programme, index) => (
                            <ListItem key={index} divider className={"mentorListItem"}>
                                <Grid container>
                                    <Grid item>
                                        <ListItemText><Typography >{index+1+"."+programme.name}</Typography></ListItemText>
                                    </Grid>
                                </Grid>
                            </ListItem>
                        ))}
                        {this.state.showAddProgrammeField && (
                            <ListItem divider className={"mentorListHeader"}>
                                <TextField
                                    fullWidth
                                    onChange={(e)=>this.setState({newProgrammeValue:e.target.value})}
                                    error={!this.state.validNewProgramme}
                                    helperText={!this.state.validNewProgramme && <FormattedMessage id={"fieldError1"}/>}
                                />
                                <Button onClick={this.onAddNewProgramme}>
                                    <AddCircleOutlineIcon/>
                                    <FormattedMessage id={"programmes.add"}/>
                                </Button>
                            </ListItem>
                        )}
                    </List>
                </Paper>
            </React.Fragment>
        );
    }
}
