import React, { Component } from "react";
import axios from "axios";
import {authenticationService} from "../../_services/authenticationService";
import {
    Grid,
} from "@material-ui/core";
import "./Adminpanel.css"
import Mentors from "./Mentors";
import Programmes from "./Programmes";
import ErrorSnackbar from "../ErrorHandlers/ErrorSnackbar";


interface State {
    programmes:Programme[],
    showErrorSnackbar : boolean
}

export interface Programme{
    name:string
}

/**
 * @description A panel that contains all admin functionalities
 * @author Koen laermans
 */
export default class AdminPanel extends Component<{}, State> {

    state:State;

    constructor (props: {}){
        super(props);
        this.state = {
            programmes:[],
            showErrorSnackbar:false
        };
        this.handleAddNewEducation = this.handleAddNewEducation.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    /**
     * @description Handles the closing of the error dialog
     */
    handleClose(){
        this.setState({
            showErrorSnackbar:false
        })
    }

    /**
     * @description Fetches all required information from the database
     */
    async componentDidMount() {
        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const ProgrammesAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/programmes",
            responseType : "json",
        });


        let programmes:Programme[] = (await ProgrammesAPI.get("/")).data.data;

        this.setState({
            programmes
        });
    }

    /**
     * @description Handles the adding of a new education/programme
     * @param newProgramme: the programme name
     */
    async handleAddNewEducation(newProgramme:string){

        const AUTH_TOKEN = authenticationService.getToken();
        axios.defaults.headers.common["Authorization"] = "Bearer " + AUTH_TOKEN;

        const ProgrammesAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/programmes",
            responseType : "json",
        });
        try{
            await ProgrammesAPI.post("/",{
                programme:newProgramme
            });
        }catch (e) {
            this.setState({
                showErrorSnackbar:true
            });
            return;
        }

        this.setState({
            programmes:this.state.programmes.concat({name:newProgramme}),
        });
    }


    render() {
        return (
            <React.Fragment>
                <Grid container>
                    <Grid item><Mentors programmes={this.state.programmes}/></Grid>
                    <Grid item><Programmes programmes={this.state.programmes} handleAddNewProgramme={this.handleAddNewEducation}/></Grid>
                </Grid>
                {(this.state.showErrorSnackbar && <ErrorSnackbar message={"Something went wrong!"} handleClose={this.handleClose}/>)}
            </React.Fragment>
        );
    }

}


