import React, {CSSProperties} from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {FormattedMessage} from 'react-intl';
import IconButton from "@material-ui/core/IconButton";
import PersonIcon from '@material-ui/icons/Person';
import ExitToApp from '@material-ui/icons/ExitToApp';
import {Redirect} from "react-router-dom";
import {authenticationService} from "../../_services/authenticationService";

/**
 * Props
 */
interface IProps {}

/**
 * Account dropdown component
 */
export default function AccountDropdown(props: IProps) {

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    /**
     * Open the dropdown menu
     */
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    /**
     * Close the dropdown
     */
    const handleClose = () => {
        setAnchorEl(null);
    };

    /**
     * Logout the user
     */
    const handleLogout = () => {
        authenticationService.logout();
        window.location.reload();
        handleClose();
        return <Redirect to={'/'}/>
    };

    /**
     *  All styling properties
     */
    const styleIcon : CSSProperties = {
        marginRight : "6px",
    };

    /**
     * Render
     */
    return (
        <div>
            <IconButton onClick={handleClick} color="inherit" >
                <PersonIcon/>
            </IconButton>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted={true}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={handleLogout}>
                    <ExitToApp style={styleIcon}/>
                    <FormattedMessage id="AccountDropdown.logout"  />
                </MenuItem>
            </Menu>
        </div>
    );
}

