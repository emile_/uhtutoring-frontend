import React, {Component, ReactElement} from "react";
import axios from "axios";
import {Paper} from "@material-ui/core";
import "./ConfirmEmail.css"
import {FormattedMessage} from "react-intl";


interface State {
    isValid:boolean | undefined
}

/**
 * @author Koen Laermans
 * @description Confirms an email
 */
export default class ConfirmEmail extends Component<{}, State> {

    state:State;

    constructor (props: {}){
        super(props);
        this.state = {
            isValid:undefined,
        };
        this.getResponseLayout = this.getResponseLayout.bind(this);
    }


    /**
     * @description confirms the email
     */
    async componentDidMount() {
        let params = new URLSearchParams(window.location.search);
        let hash = params.get("hash");

        if (hash === undefined || hash === null){
            this.setState({
                isValid:false
            });
            return;
        }

        const UsersAPI = axios.create({
            baseURL : process.env.REACT_APP_BACKEND_API_URL + "/users",
            responseType : "json",
        });

        UsersAPI.post("/confirm", {
            hash:hash
        }).then(response => {
            this.setState({
                isValid:true
            });
        }).catch(error => {
            this.setState({
                isValid:false
            });
        });
    }


    /**
     * @description returns the layout of the component
     */
    getResponseLayout(): ReactElement{
        switch (this.state.isValid) {
            case undefined:
                return <div/>;
            case false:
                return (<Paper className={"confirmation confirmationError"}><FormattedMessage id={"confirmEmail.fail"}/></Paper>);
            case true:
                return (
                    <Paper className={"confirmation confirmationSuccess"}>
                        <div><FormattedMessage id={"confirmEmail.success"}/></div>
                    </Paper>
                );
        }
    }


    render() {
        return (
            this.getResponseLayout()
        );
    }
}

