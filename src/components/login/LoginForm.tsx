import React, {ChangeEvent, Component, CSSProperties} from 'react';
import {createMuiTheme} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import {FormattedMessage} from 'react-intl';
import Container from '@material-ui/core/Container';
import {InputLabel, TextField, Typography} from "@material-ui/core";
import {authenticationService} from "../../_services/authenticationService";
import {Redirect} from "react-router-dom";

/**
 * State
 */
interface IState {
    emailValue : string
    passwordValue : string
    maxLengthEmail : number
    maxLengthPassword : number
    passwordIncorrect : boolean
    emailIncorrect : boolean
    loggedIn : boolean
    redirectToRegistration : boolean
}

/**
 * LoginForm component
 */
export default class LoginForm extends Component<any, IState> {

    /**
     * Constructor
     */
    constructor(props : any) {
        super(props);
        this.state = {
            emailValue : "",
            passwordValue : "",
            maxLengthEmail : 70,
            maxLengthPassword : 35,
            passwordIncorrect : false,
            emailIncorrect : false,
            loggedIn : authenticationService.getToken()!=null,
            redirectToRegistration : false,
        };

        this.onChange = this.onChange.bind(this);
        this.postLogin = this.postLogin.bind(this);
        this.passwordFieldHelperText = this.passwordFieldHelperText.bind(this);
        this.emailFieldHelperText = this.emailFieldHelperText.bind(this);
        this.redirectToRegistration = this.redirectToRegistration.bind(this);
    }

    // Create a theme
    theme = createMuiTheme();

    /**
     * All styling properties
     */
    styleLoginForm : CSSProperties = {
        boxShadow: '1px 3px 2px -2px rgba(0,0,0,0.2), 1px 2px 2px 1px rgba(0,0,0,0.14), 1px 2px 4px 1px rgba(0,0,0,0.12)'
    };

    styleButton : CSSProperties = {
        marginTop: this.theme.spacing(2),
        marginBottom: this.theme.spacing(2),
        marginLeft: this.theme.spacing(1),
    };

    styleForms: CSSProperties = {
        textAlign: 'left',
        marginTop: this.theme.spacing(2),
        marginLeft: this.theme.spacing(4),
        marginBottom: this.theme.spacing(1),
    };

    styleHeaders: CSSProperties = {
        paddingTop: this.theme.spacing(2),
        textAlign: 'center',
    };

    styleTextField : CSSProperties = {
        marginLeft: this.theme.spacing(1),
        marginRight: this.theme.spacing(1),
        width: 400,
    };

    styleInputLabel : CSSProperties = {
        paddingTop: this.theme.spacing(2),
        paddingBottom: this.theme.spacing(0.2),
    };

    styleRedirect : CSSProperties = {
        textDecorationLine : 'underline',
        cursor : 'pointer',
        textAlign: 'center',
        paddingBottom: this.theme.spacing(2),
    };

    /**
     * Update the value of a field.
     * @param field target field
     * @param value target value
     */
    setValue = (field : string, value : string) => {
        switch (field)
        {

            case 'email':
                // Max length for an email address reached.
                if(value.length > this.state.maxLengthEmail){
                    return
                }
                this.setState({emailIncorrect : false});
                this.setState({emailValue : value});
                break;

            case 'password':
                // Max length for a password reached.
                if(value.length > this.state.maxLengthPassword){
                    return
                }
                this.setState({passwordIncorrect : false});
                this.setState({passwordValue : value});
                break;

            default:
                break;

        }
    };

    /**
     * Post the login.
     */
    async postLogin() {
        let invalidField : boolean = false;

        // Email field is empty.
        if(this.state.emailValue === ""){
            invalidField = true;
            this.setState({"emailIncorrect" : true});
        }

        // Password field is empty.
        if(this.state.passwordValue === ""){
            invalidField = true;
            this.setState({"passwordIncorrect" : true});
        }

        // There are no invalid fields.
        if(!invalidField){
            let statusCode : number | null = await authenticationService.login(this.state.emailValue,this.state.passwordValue);

            if (!statusCode){
            }
            // Login succeeded.
            else if (statusCode === 200){
                window.location.reload();
            }
            // Login failed.
            else if (statusCode === 400){
                    this.setState({passwordIncorrect : true})
            }
            // Login failed.
            else if (statusCode === 403){
                this.setState({emailIncorrect : true})
            }
            else {

            }
        }

    };

    /**
     * Defines helper text for the email field
     */
    emailFieldHelperText(){

            // Empty email field.
            if(this.state.emailIncorrect){
                if(this.state.emailValue.length === 0) {
                    return <FormattedMessage id="LoginForm.emptyField"/>;
                }
                else {
                    return <FormattedMessage id="LoginForm.accountNotValidated"/>;
                }
            }

            // Everything is alright.
            else {
                return "";
            }
    };

    /**
     * Defines helper text for the password field
     */
    passwordFieldHelperText(){

        // Something is incorrect.
        if(this.state.passwordIncorrect){

            // Password field is empty.
            if(this.state.passwordValue === ""){
                return <FormattedMessage id="LoginForm.emptyField"/>;
            }

            // Incorrect credentials.
            else {
                return <FormattedMessage id="LoginForm.incorrectCredentials"/>;
            }
        }
        // Everything is alright.
        else {
            return "";
        }
    };

    /**
     * When the input of a field has changed.
     * @param event event from input field
     */
    onChange = (event : ChangeEvent<HTMLInputElement>) =>{
        let targetField : string = event.currentTarget.name;
        let targetValue : string = event.currentTarget.value;
        this.setValue(targetField,targetValue);
    };

    /**
     * Redirect the user to the registration
     */
    redirectToRegistration() {
        this.setState({redirectToRegistration : true});
    }

    /**
     * Render
     */
    render() {

        // Redirect if logged in
        if(this.state.loggedIn || this.props.location.pathname !== "/"){
            return <Redirect to={'/'}/>
        }

        // Redirect to registration
        if(this.state.redirectToRegistration){
            return <Redirect to='/registration'/>
        }

        /**
         * Default render
         */
        return (
            <React.Fragment>
                <Container maxWidth="sm"  style={this.styleLoginForm}>
                        <h1 style={this.styleHeaders}><FormattedMessage id={"LoginForm.header"}/></h1>
                        <Divider variant="middle"/>
                        <div style={this.styleForms}>
                            <InputLabel style={this.styleInputLabel}><FormattedMessage id="LoginForm.emailField"/></InputLabel>
                            <TextField
                                style={this.styleTextField}
                                error={this.state.emailIncorrect}
                                value={this.state.emailValue}
                                helperText={this.emailFieldHelperText()}
                                name="email"
                                onChange={this.onChange}
                                margin="normal"
                                fullWidth={true}
                                variant="outlined"
                            />
                            <InputLabel style={this.styleInputLabel}><FormattedMessage id="LoginForm.passwordField"/></InputLabel>
                            <TextField
                                type="password"
                                style={this.styleTextField}
                                error={this.state.passwordIncorrect}
                                value={this.state.passwordValue}
                                helperText={this.passwordFieldHelperText()}
                                name="password"
                                onChange={this.onChange}
                                margin="normal"
                                fullWidth={true}
                                variant="outlined"
                                onKeyDown={(e)=>{if (e.key === 'Enter'){this.postLogin()}}}
                            /><br/>
                            <Button
                                variant="contained"
                                color="primary"
                                style={this.styleButton}
                                onClick={this.postLogin}
                            >
                                <FormattedMessage id={"LoginForm.button"}/>
                            </Button>
                        </div>
                    <Typography onClick={this.redirectToRegistration} style={this.styleRedirect}>
                        <FormattedMessage id={"LoginForm.registrationRedirection"}/>
                    </Typography>
                </Container>
            </React.Fragment>
        );
    }
}
